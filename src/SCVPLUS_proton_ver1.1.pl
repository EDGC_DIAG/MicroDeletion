#!usr/bin/perl
use strict;
use warnings;

#############################################################################################################
#              																					  			#
#This script SCVPLUS_proton_ver1.2.pl determines the SCV-score, given the UniqBam File.			  			#
#																								  			#
#																								  			#
# Please, Provide the folowwing 3 arguments to the perl command;									  		#
#																								  			#
# 1. Mapped_bam_File : Name/location of the UniqBam file obtained by samtools -bq command.	      			#
# 2. Sample_ID: UniqID for this sample (a prefix to all the intermediate and result file)				  	#
# 3. OutDir: /Path/to/OutDir																				#
#																										  	#
# Example Run:																							  	#
# perl SCVPLUS_proton_ver1.2.pl ./SampleID.uniq.bam SampleID /Path/to/OutDir								#
#																										  	#
#																										  	#
#############################################################################################################

use File::Basename;
my $path = "$0";
my($filename, $MyPath, $suffix) = fileparse($path);
$MyPath =~ s/\/$//;
$MyPath =~ s/src/data/i;
#
#
#####################################################################################################
#																								 	#
#																								 	#
###### Tools and reference hg19.2bit file location. Please check this command and reference file	#
#      depending on the server																		#
#																									#
#Common Arguments												   									#	
#																								 	#
my $computeGCBias = "/BIO/app/deeptools/computeGCBias";												#
my $correctGCBias = "/BIO/app/deeptools/correctGCBias";												#
my $BedTools = "/opt/bedtools2/bin/bedtools";											 	    	#
my $SAMTools1_2 = "/BIO/app/samtools-1.2/samtools";										 	    	#
#																								 	#
# Specific Aeguments ###																		 	#
my $RefGen2bit = "$MyPath/hg19.2bit";																#
my $cytoBand = "$MyPath/cytoBand_proton_v1.1.txt";													#
my $cytoBand_SD = "$MyPath/cytoBand_proton_SD_v1.1.txt";											#
#####################################################################################################
#
my $Mapped_bam_File = $ARGV[0];
my $Sample_ID = $ARGV[1];
my $OutDir = $ARGV[2];
chomp $Mapped_bam_File;
chomp $Sample_ID;
chomp $OutDir;
#
#
#
`mkdir "$OutDir/$Sample_ID"`;
#
#
#############################################################################################################
#
### GC Correction using deeptools's computerGCBias and correctGCBias
### Check the location of both the command scripts and hg19.2bit file while running the script
#
#
#It's a computer intensive process and might take ~5-10 min for a sample with ~10million sequence reads (Depending on the #CPU)
#
#
`$computeGCBias -b $Mapped_bam_File --numberOfProcessors=6 --effectiveGenomeSize 2451960000 --genome $RefGen2bit --fragmentLength 200 --GCbiasFrequenciesFile "$OutDir/$Sample_ID/$Sample_ID".frequencies.txt`;
`$correctGCBias -b $Mapped_bam_File --numberOfProcessors=6 --effectiveGenomeSize 2451960000 --genome $RefGen2bit --GCbiasFrequenciesFile "$OutDir/$Sample_ID/$Sample_ID".frequencies.txt --correctedFile "$OutDir/$Sample_ID/$Sample_ID".GCcorrected.bam`;
#
#
chdir "/$OutDir/$Sample_ID";
#
#
#`rm $Sample_ID.GCcorrected.bam`;
#`rm $Sample_ID.GCcorrected.bam.bai`;
#`rm $Sample_ID.frequencies.txt`;
#
#
#
#############################################################################################################





##### Count the Genomic Reads in the sample for each cytoBand ###############################################
#
#
`$BedTools multicov -bams "$OutDir/$Sample_ID/$Sample_ID".GCcorrected.bam -bed $cytoBand > "$Sample_ID".Cytoband_ReadCount.txt`;
#
open my $OFILECB, '>>', $Sample_ID.'.Cytoband_ReadCount.txt' or die "Cannot create file for output: $!";
#
my $MSYcount= `$SAMTools1_2 view -c -@ 6 "$OutDir/$Sample_ID/$Sample_ID".GCcorrected.bam chrY:2800001-2850000 chrY:6850001-6900000 chrY:6900001-6950000 chrY:7250001-7300000 chrY:7550001-7600000 chrY:7850001-7900000 chrY:7900001-7950000 chrY:8350001-8400000 chrY:8600001-8650000 chrY:8850001-8900000 chrY:9850001-9900000 chrY:14400001-14450000 chrY:15550001-15600000 chrY:15800001-15850000 chrY:16400001-16450000 chrY:16850001-16900000 chrY:17100001-17150000 chrY:17500001-17550000 chrY:17750001-17800000 chrY:17850001-17900000 chrY:18150001-18200000 chrY:18650001-18700000 chrY:18850001-18900000 chrY:19150001-19200000 chrY:19500001-19550000 chrY:20800001-20850000 chrY:21650001-21700000 chrY:22050001-22100000 chrY:22700001-22750000 chrY:22750001-22800000 chrY:22800001-22850000`;
print $OFILECB "MSY\tMSY\tMSY\tMSY\tMSY\t$MSYcount";
#
#close the filehandle and exit
#
close $OFILECB;
#
#############################################################################################################





##### Genomic Reads in the sample for desired chromosome / disease related region ###########################
#
#
my $RC_chr1 = 0;
my $RC_chr2 = 0;
my $RC_chr3 = 0;
my $RC_chr4 = 0;
my $RC_chr5 = 0;
my $RC_chr6 = 0;
my $RC_chr7 = 0;
my $RC_chr8 = 0;
my $RC_chr9 = 0;
my $RC_chr10 = 0;
my $RC_chr11 = 0;
my $RC_chr12 = 0;
my $RC_chr13 = 0;
my $RC_chr14 = 0;
my $RC_chr15 = 0;
my $RC_chr16 = 0;
my $RC_chr17 = 0;
my $RC_chr18 = 0;
my $RC_chr19 = 0;
my $RC_chr20 = 0;
my $RC_chr21 = 0;
my $RC_chr22 = 0;
my $RC_chrX = 0;
my $RC_chrY = 0;
#
my $RC_T9p = 0;
my $RC_T21q = 0;
my $RC_T22 = 0;
#
#
# Below are the Microdeletion region 20160720
#
#
my $RC_1p36DS = 0;
my $RC_1p36DS_1 = 0;
my $RC_1p36DS_2 = 0;
my $RC_1p36DS_3 = 0;
my $RC_1p36DS_4 = 0;
my $RC_2q33DS = 0;
my $RC_2q33DS_1 = 0;
my $RC_2q33DS_2 = 0;
my $RC_2q33DS_3 = 0;
my $RC_2q33DS_4 = 0;
my $RC_2q33DS_5 = 0;
my $RC_2q33DS_6 = 0;
my $RC_2q33DS_7 = 0;
my $RC_WolfHirschhorn = 0;
my $RC_WolfHirschhorn_1 = 0;
my $RC_WolfHirschhorn_2 = 0;
my $RC_WolfHirschhorn_3 = 0;
my $RC_WolfHirschhorn_4 = 0;
my $RC_WolfHirschhorn_5 = 0;
my $RC_WolfHirschhorn_6 = 0;
my $RC_WolfHirschhorn_7 = 0;
my $RC_CriDuChat = 0;
my $RC_CriDuChat_1 = 0;
my $RC_CriDuChat_2 = 0;
my $RC_CriDuChat_3 = 0;
my $RC_CriDuChat_4 = 0;
my $RC_CriDuChat_5 = 0;
my $RC_CriDuChat_6 = 0;
my $RC_CriDuChat_7 = 0;
my $RC_CriDuChat_8 = 0;
my $RC_CriDuChat_9 = 0;
my $RC_WilliamsBeuren_1 = 0;
my $RC_Jacobsen = 0;
my $RC_Jacobsen_4 = 0;
my $RC_Jacobsen_5 = 0;
my $RC_Jacobsen_6 = 0;
my $RC_Jacobsen_7 = 0;
my $RC_PraderWilli = 0;
my $RC_PraderWilli_2 = 0;
my $RC_PraderWilli_3 = 0;
my $RC_DiGeorge_1 = 0;
#
#############################################################################################################





#############################################################################################################
#
#
open (IN, "$Sample_ID.Cytoband_ReadCount.txt") or die;
# go through the file line by line    
while (my $line = <IN>)
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	#
    # split the current line on tabs
    my @columns = split(/\t/, $line);
	#
	chomp $columns[3];
	chomp $columns[5];
	#
	open my $OFILE, '>>', $Sample_ID.'.CytoBand_GRCount.txt' or die "Cannot create file for output: $!";
	print $OFILE "$columns[3]\t$columns[5]\n";	
	#
	if ($columns[3] =~ m/(^01p|01q)/)	{	$RC_chr1+=$columns[5];	}
	if ($columns[3] =~ m/(^02p|02q)/)	{	$RC_chr2+=$columns[5];	}	
	if ($columns[3] =~ m/(^03p|03q)/)	{	$RC_chr3+=$columns[5];	}
	if ($columns[3] =~ m/(^04p|04q)/)	{	$RC_chr4+=$columns[5];	}
	if ($columns[3] =~ m/(^05p|05q)/)	{	$RC_chr5+=$columns[5];	}
	if ($columns[3] =~ m/(^06p|06q)/)	{	$RC_chr6+=$columns[5];	}
	if ($columns[3] =~ m/(^07p|07q)/)	{	$RC_chr7+=$columns[5];	}
	if ($columns[3] =~ m/(^08p|08q)/)	{	$RC_chr8+=$columns[5];	}
	if ($columns[3] =~ m/(^09p|09q)/)	{	$RC_chr9+=$columns[5];	}
	if ($columns[3] =~ m/(^10p|10q)/)	{	$RC_chr10+=$columns[5];	}
	if ($columns[3] =~ m/(^11p|11q)/)	{	$RC_chr11+=$columns[5];	}
	if ($columns[3] =~ m/(^12p|12q)/)	{	$RC_chr12+=$columns[5];	}
	if ($columns[3] =~ m/(^13p|13q)/)	{	$RC_chr13+=$columns[5];	}
	if ($columns[3] =~ m/(^14p|14q)/)	{	$RC_chr14+=$columns[5];	}
	if ($columns[3] =~ m/(^15p|15q)/)	{	$RC_chr15+=$columns[5];	}
	if ($columns[3] =~ m/(^16p|16q)/)	{	$RC_chr16+=$columns[5];	}
	if ($columns[3] =~ m/(^17p|17q)/)	{	$RC_chr17+=$columns[5];	}
	if ($columns[3] =~ m/(^18p|18q)/)	{	$RC_chr18+=$columns[5];	}
	if ($columns[3] =~ m/(^19p|19q)/)	{	$RC_chr19+=$columns[5];	}
	if ($columns[3] =~ m/(^20p|20q)/)	{	$RC_chr20+=$columns[5];	}
	if ($columns[3] =~ m/(^21p|21q)/)	{	$RC_chr21+=$columns[5];	}
	if ($columns[3] =~ m/(^22p|22q)/)	{	$RC_chr22+=$columns[5];	}
	if ($columns[3] =~ m/(^Xp|Xq)/)	{	$RC_chrX+=$columns[5];	}
	if ($columns[3] =~ m/(^Yp|Yq)/)	{	$RC_chrY+=$columns[5];	}
	##if ($columns[3] =~ m/MSY/)	{	$RC_MSY+=$columns[5];	}
#
#
###### Trisomy 9, 16 and 22 region
#	
#
	if ($columns[3] =~ m/^09p/)	{	$RC_T9p+=$columns[5];	}
	if ($columns[3] =~ m/^21q/)	{	$RC_T21q+=$columns[5];	}
	if ($columns[3] =~ m/(22p13|22p12|22p11.2|22p11.1|22q11.1|22q11.21|22q11.22|22q11.23|11q23.3|11q24.1|11q24.2|11q24.3|11q25)/)	{	$RC_T22+=$columns[5];	}	

#
#
####### Microdeletion Region
#
#	Region Update on 160720
	#if ($columns[3] =~ m/(01p36.33|01p36.32|01p36.31|01p36.23|01p36.22|01p36.21|01p36.13|01p36.12|01p36.11)/)
	if ($columns[3] =~ m/(01p36.33|01p36.32|01p36.31|01p36.23)/)	{	$RC_1p36DS+=$columns[5];	}
	if ($columns[3] eq "01p36.33")	{	$RC_1p36DS_1+=$columns[5];	}
	if ($columns[3] eq "01p36.32")	{	$RC_1p36DS_2+=$columns[5];	}
	if ($columns[3] eq "01p36.31")	{	$RC_1p36DS_3+=$columns[5];	}
	if ($columns[3] eq "01p36.23")	{	$RC_1p36DS_4+=$columns[5];	}
	
	#if ($columns[3] =~ m/(02q31.1|02q31.2|02q31.3|02q32.1|02q32.2|02q32.3|02q33.1|02q33.2|02q33.3)/)	### Kept on one type of 2q33 Deletion. Other is removed on 20160720
	if ($columns[3] =~ m/(02q31.1|02q31.2|02q31.3|02q32.1|02q32.2|02q32.3|02q33.1|02q33.2|02q33.3)/)	{	$RC_2q33DS+=$columns[5];	}
	if ($columns[3] eq "02q31.1")	{	$RC_2q33DS_1+=$columns[5];	}
	if ($columns[3] eq "02q31.2")	{	$RC_2q33DS_2+=$columns[5];	}
	if ($columns[3] eq "02q31.3")	{	$RC_2q33DS_3+=$columns[5];	}
	if ($columns[3] eq "02q32.1")	{	$RC_2q33DS_4+=$columns[5];	}
	if ($columns[3] eq "02q32.2")	{	$RC_2q33DS_5+=$columns[5];	}
	if ($columns[3] eq "02q32.3")	{	$RC_2q33DS_6+=$columns[5];	}
	if ($columns[3] eq "02q33.1")	{	$RC_2q33DS_7+=$columns[5];	}
	
	#if ($columns[3] =~ m/04p16.3/)
	if ($columns[3] =~ m/(04p16.3|04p16.2|04p16.1|04p15.33|04p15.32|04p15.31|04p15.2)/)	{	$RC_WolfHirschhorn+=$columns[5];	}
	if ($columns[3] eq "04p16.3")	{	$RC_WolfHirschhorn_1+=$columns[5];	}
	if ($columns[3] eq "04p16.2")	{	$RC_WolfHirschhorn_2+=$columns[5];	}
	if ($columns[3] eq "04p16.1")	{	$RC_WolfHirschhorn_3+=$columns[5];	}
	if ($columns[3] eq "04p15.33")	{	$RC_WolfHirschhorn_4+=$columns[5];	}
	if ($columns[3] eq "04p15.32")	{	$RC_WolfHirschhorn_5+=$columns[5];	}
	if ($columns[3] eq "04p15.31")	{	$RC_WolfHirschhorn_6+=$columns[5];	}
	if ($columns[3] eq "04p15.2")	{	$RC_WolfHirschhorn_7+=$columns[5];	}

	#if ($columns[3] =~ m/05p15.2/)
	if ($columns[3] =~ m/(05p15.33|05p15.32|05p15.31|05p15.2|05p15.1|05p14.3|05p14.2|05p14.1|05p13.3)/)	{	$RC_CriDuChat+=$columns[5];	}
	if ($columns[3] eq "05p15.33")	{	$RC_CriDuChat_1+=$columns[5];	}
	if ($columns[3] eq "05p15.32")	{	$RC_CriDuChat_2+=$columns[5];	}
	if ($columns[3] eq "05p15.31")	{	$RC_CriDuChat_3+=$columns[5];	}
	if ($columns[3] eq "05p15.2")	{	$RC_CriDuChat_4+=$columns[5];	}
	if ($columns[3] eq "05p15.1")	{	$RC_CriDuChat_5+=$columns[5];	}
	if ($columns[3] eq "05p14.3")	{	$RC_CriDuChat_6+=$columns[5];	}
	if ($columns[3] eq "05p14.2")	{	$RC_CriDuChat_7+=$columns[5];	}
	if ($columns[3] eq "05p14.1")	{	$RC_CriDuChat_8+=$columns[5];	}
	if ($columns[3] eq "05p13.3")	{	$RC_CriDuChat_9+=$columns[5];	}

	#if ($columns[3] eq "07q11.23") ###Update in CytoBand_Region File "cytoBand_160720.txt": Line-350-351 on 160720
	if ($columns[3] eq "07q11.23_1")	{	$RC_WilliamsBeuren_1+=$columns[5];	}
	
	#if ($columns[3] =~ m/(11q23.1|11q23.2|11q23.3|11q24.1|11q24.2|11q24.3)/)
	if ($columns[3] =~ m/(11q23.1|11q23.2|11q23.3|11q24.1|11q24.2|11q24.3|11q25)/)	{	$RC_Jacobsen+=$columns[5];	}
	if ($columns[3] eq "11q24.1")	{	$RC_Jacobsen_4+=$columns[5];	}
	if ($columns[3] eq "11q24.2")	{	$RC_Jacobsen_5+=$columns[5];	}
	if ($columns[3] eq "11q24.3")	{	$RC_Jacobsen_6+=$columns[5];	}
	if ($columns[3] eq "11q25")	{	$RC_Jacobsen_7+=$columns[5];	}
	
	#if ($columns[3] =~ m/(15q11.1|15q11.2|15q12)/)
	if ($columns[3] =~ m/(15q11.1|15q11.2|15q12|15q13.1)/)	{	$RC_PraderWilli+=$columns[5];	}
	if ($columns[3] eq "15q11.2")	{	$RC_PraderWilli_2+=$columns[5];	}
	if ($columns[3] eq "15q12")	{	$RC_PraderWilli_3+=$columns[5];	}

	#if ($columns[3] =~ m/(22q11.21|22q11.22|22q11.23)/)	### No Change on 160720
	if ($columns[3] eq "22q11.21")	{	$RC_DiGeorge_1+=$columns[5];	}
}
# close the filehandle and exit
close IN;
#
#
open my $OFILE, '>>', $Sample_ID.'.CytoBand_GRCount.txt' or die "Cannot create file for output: $!";
print $OFILE "Chr1\t$RC_chr1\n";
print $OFILE "Chr2\t$RC_chr2\n";
print $OFILE "Chr3\t$RC_chr3\n";
print $OFILE "Chr4\t$RC_chr4\n";
print $OFILE "Chr5\t$RC_chr5\n";
print $OFILE "Chr6\t$RC_chr6\n";
print $OFILE "Chr7\t$RC_chr7\n";
print $OFILE "Chr8\t$RC_chr8\n";
print $OFILE "Chr9\t$RC_chr9\n";
print $OFILE "T9p\t$RC_T9p\n";
print $OFILE "Chr10\t$RC_chr10\n";
print $OFILE "Chr11\t$RC_chr11\n";
print $OFILE "Chr12\t$RC_chr12\n";
print $OFILE "Chr13\t$RC_chr13\n";
print $OFILE "Chr14\t$RC_chr14\n";
print $OFILE "Chr15\t$RC_chr15\n";
print $OFILE "Chr16\t$RC_chr16\n";
print $OFILE "Chr17\t$RC_chr17\n";
print $OFILE "Chr18\t$RC_chr18\n";
print $OFILE "Chr19\t$RC_chr19\n";
print $OFILE "Chr20\t$RC_chr20\n";
print $OFILE "Chr21\t$RC_chr21\n";
print $OFILE "T21q\t$RC_T21q\n";
print $OFILE "Chr22\t$RC_chr22\n";
print $OFILE "T22\t$RC_T22\n";
print $OFILE "ChrX\t$RC_chrX\n";
print $OFILE "ChrY\t$RC_chrY\n";
##print $OFILE "MSY\t$RC_MSY\n";
#
print $OFILE "MD_1p36DS\t$RC_1p36DS\n";
print $OFILE "MD_1p36DS_1\t$RC_1p36DS_1\n";
print $OFILE "MD_1p36DS_2\t$RC_1p36DS_2\n";
print $OFILE "MD_1p36DS_3\t$RC_1p36DS_3\n";
print $OFILE "MD_1p36DS_4\t$RC_1p36DS_4\n";
print $OFILE "MD_2q33DS\t$RC_2q33DS\n";
print $OFILE "MD_2q33DS_1\t$RC_2q33DS_1\n";
print $OFILE "MD_2q33DS_2\t$RC_2q33DS_2\n";
print $OFILE "MD_2q33DS_3\t$RC_2q33DS_3\n";
print $OFILE "MD_2q33DS_4\t$RC_2q33DS_4\n";
print $OFILE "MD_2q33DS_5\t$RC_2q33DS_5\n";
print $OFILE "MD_2q33DS_6\t$RC_2q33DS_6\n";
print $OFILE "MD_2q33DS_7\t$RC_2q33DS_7\n";
print $OFILE "WolfHirschhorn\t$RC_WolfHirschhorn\n";
print $OFILE "WolfHirschhorn_1\t$RC_WolfHirschhorn_1\n";
print $OFILE "WolfHirschhorn_2\t$RC_WolfHirschhorn_2\n";
print $OFILE "WolfHirschhorn_3\t$RC_WolfHirschhorn_3\n";
print $OFILE "WolfHirschhorn_4\t$RC_WolfHirschhorn_4\n";
print $OFILE "WolfHirschhorn_5\t$RC_WolfHirschhorn_5\n";
print $OFILE "WolfHirschhorn_6\t$RC_WolfHirschhorn_6\n";
print $OFILE "WolfHirschhorn_7\t$RC_WolfHirschhorn_7\n";
print $OFILE "CriDuChat\t$RC_CriDuChat\n";
print $OFILE "CriDuChat_1\t$RC_CriDuChat_1\n";
print $OFILE "CriDuChat_2\t$RC_CriDuChat_2\n";
print $OFILE "CriDuChat_3\t$RC_CriDuChat_3\n";
print $OFILE "CriDuChat_4\t$RC_CriDuChat_4\n";
print $OFILE "CriDuChat_5\t$RC_CriDuChat_5\n";
print $OFILE "CriDuChat_6\t$RC_CriDuChat_6\n";
print $OFILE "CriDuChat_7\t$RC_CriDuChat_7\n";
print $OFILE "CriDuChat_8\t$RC_CriDuChat_8\n";
print $OFILE "CriDuChat_9\t$RC_CriDuChat_9\n";
print $OFILE "WilliamsBeuren_1\t$RC_WilliamsBeuren_1\n";
print $OFILE "Jacobsen\t$RC_Jacobsen\n";
print $OFILE "Jacobsen_4\t$RC_Jacobsen_4\n";
print $OFILE "Jacobsen_5\t$RC_Jacobsen_5\n";
print $OFILE "Jacobsen_6\t$RC_Jacobsen_6\n";
print $OFILE "Jacobsen_7\t$RC_Jacobsen_7\n";
print $OFILE "PraderWilli\t$RC_PraderWilli\n";
print $OFILE "PraderWilli_2\t$RC_PraderWilli_2\n";
print $OFILE "PraderWilli_3\t$RC_PraderWilli_3\n";
print $OFILE "DiGeorge_1\t$RC_DiGeorge_1\n";
#
close $OFILE;
#
#############################################################################################################





##### Count the total Genomic Reads in the sample ###########################################################
#
#
my $count = 0;
# open a filehandle to data.txt
open (INFile, "$Sample_ID".".CytoBand_GRCount.txt") or die;
#
# go through the file line by line    
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);

    # split the current line on tabs
    my @columns = split(/\t/, $line);
	chomp $columns[0];
	chomp $columns[1];

	for ($columns[0] =~ m/(Chr1$|Chr2$|Chr3$|Chr4$|Chr5$|Chr6$|Chr7$|Chr8$|Chr9$|Chr10$|Chr11$|Chr12$|Chr13$|Chr14$|Chr15$|Chr16$|Chr17$|Chr18$|Chr19$|Chr20$|Chr21$|Chr22$|ChrX$|ChrY$)/)
	{
	$count += $columns[1];
	}
}
#
# close the filehandle and exit
close INFile;
#
open my $OFILE, '>>', $Sample_ID.'.CytoBand_GRCount.txt' or die "Cannot create file for output: $!";
print $OFILE "TotalReads\t$count\n";	
#
#############################################################################################################





##### Count %Genome Reads for each chromosomes region in the sample #########################################
#
# open a filehandle to data.txt
#
open (INFile, "$Sample_ID".".CytoBand_GRCount.txt") or die;
open my $OFILE2, '>', $Sample_ID.'.CytoBand_Percent_GR_Count.txt' or die "Cannot create file for output: $!";
#
#print $OFILE2 "CHR\t#Reads\t%chr\n";
# go through the file line by line    
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);

    # split the current line on tabs
    my @columns = split(/\t/, $line);

	chomp $columns[0];
	chomp $columns[1];	

	$columns[2] = $columns[1]/$count;
	$columns[3] = $columns[2]*100;
	
	chomp $columns[2];
	chomp $columns[3];

	print $OFILE2 "$columns[0]\t$columns[1]\t$columns[3]\n";
}

# close the filehandle and exit
close $OFILE2;
close INFile;
#
#############################################################################################################





##### Count the Divident in the sample ######################################################################
#
#
my $Divident = 0;
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_Percent_GR_Count.txt") or die;
#
# go through the file line by line    
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
    # split the current line on tabs
    my @columns = split(/\t/, $line);
	
	chomp $columns[0];
	chomp $columns[1];
	chomp $columns[2];

	if ($columns[0] =~ m/(Chr2$|Chr3$|Chr4$|Chr5$|Chr6$|Chr14$|Chr15$|Chr17$|Chr19$|Chr20$)/)
	{
	$Divident += $columns[2];
	}	
}
# close the filehandle and exit
close INFile;
#
#############################################################################################################





##### Count Standardized Chromosome Read for each chromosomes in the sample ################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_Percent_GR_Count.txt") or die;
open my $OFILE_SCR, '>', $Sample_ID.'.CytoBand_SCR_Count.txt' or die "Cannot create file for output: $!";
#
#print $OFILE_SCR "CHR\t#Reads\t%chr\t%SCR\n";
#
# go through the file line by line    
#
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
    # split the current line on tabs
    my @columns = split(/\t/, $line);

	chomp $columns[0];
	chomp $columns[1];
	chomp $columns[2];

			my $SCR = $columns[2]/$Divident;
			$SCR = $SCR * 100;
			print $OFILE_SCR "$columns[0]\t$columns[1]\t$columns[2]\t$SCR\n";

}
# close the filehandle and exit
#
close $OFILE_SCR;
close INFile;
#
#############################################################################################################





### Determine the Standardized Chromosome Value (SCV) for all the region of Interest in cytoBand_SD.txt file#
##### Including\ the MicroDeletion ragion####
#
# open a filehandle to data.txt
open (INSDFile, "$cytoBand_SD") or die;
open (INFile, "$Sample_ID.CytoBand_SCR_Count.txt") or die;
open my $OFILE_SCV, '>', $Sample_ID.'.CytoBand_SCV.txt' or die "Cannot create file for output: $!";
print $OFILE_SCV "chr\t#Reads\t%chr\t%SCR\tSCV\n";
#
#
# go through the file line by line
while (my $SDline = <INSDFile>)
{
	chomp ($SDline);
	chop($SDline) if ($SDline =~ m/\r$/);
	# split the current line on tabs
	my @SDcolumns = split(/\t/, $SDline);
	chomp $SDcolumns[0];
	chomp $SDcolumns[1];
	chomp $SDcolumns[2];
	
	while (my $line = <INFile>)
	{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
    # split the current line on tabs
    my @columns = split(/\t/, $line);
	chomp $columns[0];
	chomp $columns[1];
	chomp $columns[2];
	chomp $columns[3];
		if($SDcolumns[0] eq $columns[0])
		{
		$columns[4] = ($columns[3]-$SDcolumns[1])/$SDcolumns[2];
		chomp $columns[4];
		print $OFILE_SCV "$columns[0]\t$columns[1]\t$columns[2]\t$columns[3]\t$columns[4]\n";
		last;
		}
	}
}
#	
# close the filehandle and exit
#
close $OFILE_SCV;
close INFile;	
#
#
#############################################################################################################





##### Report the Autosomal chromosomal Anuepoidy ### Trisomy 9 #############################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line    
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);

	for ($columns[0] =~ m/(T9p)/)
	{
	chomp $columns[0];
	chomp $columns[4];
	
	if ($columns[4] >= 8)
		{
		print $OFILE4 "Trisomy9\tDETECTED\n";
		print $OFILE4 "$columns[0]\t$columns[4]\tDETECTED\t\t[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]\n";
		}
		elsif ($columns[4] >= 6 && $columns[4] < 8)
			{
			print $OFILE4 "Trisomy9\tSUSPECTED\n";
			print $OFILE4 "$columns[0]\t$columns[4]\tSUSPECTED\t\t[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]\n";
			}
			else
				{
				print $OFILE4 "Trisomy9\tNORMAL\n";
				print $OFILE4 "$columns[0]\t$columns[4]\tNORMAL\t\t[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]\n";
				}
	}



}
print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################





##### Report the Autosomal chromosomal Anuepoidy ### Trisomy 13 #############################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line    
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);

	for ($columns[0] =~ m/(Chr13)/)
	{
	chomp $columns[0];
	chomp $columns[4];
	
	if ($columns[4] >= 8)
		{
		print $OFILE4 "Trisomy13\tDETECTED\n";
		print $OFILE4 "$columns[0]\t$columns[4]\tDETECTED\t\t[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]\n";
		}
		elsif ($columns[4] >= 6 && $columns[4] < 8)
			{
			print $OFILE4 "Trisomy13\tSUSPECTED\n";
			print $OFILE4 "$columns[0]\t$columns[4]\tSUSPECTED\t\t[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]\n";
			}
			else
				{
				print $OFILE4 "Trisomy13\tNORMAL\n";
				print $OFILE4 "$columns[0]\t$columns[4]\tNORMAL\t\t[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]\n";
				}
	}	
}
print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################





##### Report the Autosomal chromosomal Anuepoidy ### Trisomy 16 #############################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line    
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);

	for ($columns[0] =~ m/(Chr16)/)
	{
	chomp $columns[0];
	chomp $columns[4];
	
	if ($columns[4] >= 4)
		{
		print $OFILE4 "Trisomy16\tDETECTED\n";
		print $OFILE4 "$columns[0]\t$columns[4]\tDETECTED\t\t[Reference Range (Z-score): Normal: Z<3.5, Suspected: 3.5=<Z<4, Detected: Z>=4]\n";
		}
		elsif ($columns[4] >=3.5 && $columns[4] <4)
				{
				print $OFILE4 "Trisomy16\tSUSPECTED\n";
				print $OFILE4 "$columns[0]\t$columns[4]\tSUSPECTED\t\t[Reference Range (Z-score): Normal: Z<3.5, Suspected: 3.5=<Z<4, Detected: Z>=4]\n";
				}
				else
					{
					print $OFILE4 "Trisomy16\tNORMAL\n";
					print $OFILE4 "$columns[0]\t$columns[4]\tNORMAL\t\t[Reference Range (Z-score): Normal: Z<3.5, Suspected: 3.5=<Z<4, Detected: Z>=4]\n";
					}
	}	
}
print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################





##### Report the Autosomal chromosomal Anuepoidy ### Trisomy 18 #############################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line    
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);

	for ($columns[0] =~ m/(Chr18)/)
	{
	chomp $columns[0];
	chomp $columns[4];
	
	if ($columns[4] >= 8)
		{
		print $OFILE4 "Trisomy18\tDETECTED\n";
		print $OFILE4 "$columns[0]\t$columns[4]\tDETECTED\t\t[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]\n";
		}
		elsif ($columns[4] >= 6 && $columns[4] < 8)
			{
			print $OFILE4 "Trisomy18\tSUSPECTED\n";
			print $OFILE4 "$columns[0]\t$columns[4]\tSUSPECTED\t\t[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]\n";
			}
			else
				{
				print $OFILE4 "Trisomy18\tNORMAL\n";
				print $OFILE4 "$columns[0]\t$columns[4]\tNORMAL\t\t[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]\n";
				}
	}	
}
print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################




##### T21 Muti-condition Check ##############################################################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line
my %chr21_SCV_Check;
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);
	
	chomp $columns[0];
	chomp $columns[4];
	for ($columns[0] =~ m/(Chr21|T21q)/)
	{
	$chr21_SCV_Check{$columns[0]} = $columns[4];
	}	
}
@T_band = keys %chr21_SCV_Check;
@T_scv = values %chr21_SCV_Check;
my $MDS_Count = 1;
if($chr21_SCV_Check{Chr21} >=5 || $chr21_SCV_Check{T21q} >=5)
	{
	print $OFILE4 "Trisomy21\tDETECTED\n";
	$MDS_Count++;
	if($T_scv[0] >=5){print $OFILE4 "$T_band[0]\t$T_scv[0]\tDETECTED\t\t[Reference Range --> Complete Chromosome 21 (Z-score): Normal: Z<4, Suspected: 4=<Z<5, Detected: Z>=5]\n";}
	if($T_scv[1] >=5){print $OFILE4 "$T_band[1]\t$T_scv[1]\tDETECTED\t\t[Reference Range --> T21q - q-arm of Chromosome 21 (Z-score): Normal: Z<4, Suspected: 4=<Z<5, Detected: Z>=5]\n";}
	}		
if(4<= $chr21_SCV_Check{Chr21} && $chr21_SCV_Check{Chr21} <5|| 4<= $chr21_SCV_Check{T21q} && $chr21_SCV_Check{T21q} <5)
	{
	if($MDS_Count<=1){print $OFILE4 "Trisomy21\tSUSPECTED\n";}
	$MDS_Count++;
	if(4<= $T_scv[0] && $T_scv[0] <4){print $OFILE4 "$T_band[0]\t$T_scv[0]\tSUSPECTED\t\t[Reference Range --> Complete Chromosome 21 (Z-score): Normal: Z<4, Suspected: 4=<Z<5, Detected: Z>=5]\n";}
	if(4<= $T_scv[1] && $T_scv[1] <4){print $OFILE4 "$T_band[1]\t$T_scv[1]\tSUSPECTED\t\t[Reference Range --> T21q - q-arm of Chromosome 21 (Z-score): Normal: Z<4, Suspected: 4=<Z<5, Detected: Z>=5]\n";}
	}
if($chr21_SCV_Check{Chr21} <4 || $chr21_SCV_Check{T21q} <4)
	{
	if($MDS_Count<=1){print $OFILE4 "Trisomy21\tNORMAL\n";}
	if($T_scv[0] <4){print $OFILE4 "$T_band[0]\t$T_scv[0]\tNORMAL\t\t[Reference Range --> Complete Chromosome 21 (Z-score): Normal: Z<4, Suspected: 4=<Z<5, Detected: Z>=5]\n";}
	if($T_scv[1] <4){print $OFILE4 "$T_band[1]\t$T_scv[1]\tNORMAL\t\t[Reference Range --> T21q - q-arm of Chromosome 21 (Z-score): Normal: Z<4, Suspected: 4=<Z<5, Detected: Z>=5]\n";}
	}
print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################





##### T22 Muti-condition Check ##############################################################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line
my %chr22_SCV_Check;
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);
	
	chomp $columns[0];
	chomp $columns[4];
	for ($columns[0] =~ m/(Chr22|T22)/)
	{
	$chr22_SCV_Check{$columns[0]} = $columns[4];
	}	
}
@T_band = keys %chr22_SCV_Check;
@T_scv = values %chr22_SCV_Check;
my $MDS_Count = 1;
if($chr22_SCV_Check{Chr22} >=3.5 || $chr22_SCV_Check{T22} >=3.5)
	{
	print $OFILE4 "Trisomy22\tDETECTED\n";
	$MDS_Count++;
	if($T_scv[0] >=3.5){print $OFILE4 "$T_band[0]\t$T_scv[0]\t\t[Reference Range --> Emanuel Syndrome (Z-score): Normal: Z<2.5, Suspected: 2.5=<Z<3.5, Detected: Z>=3.5]\n";}
	if($T_scv[1] >=3.5){print $OFILE4 "$T_band[1]\t$T_scv[1]\t\t[Reference Range --> Complete Chromosome 22 (Z-score): Normal: Z<2.5, Suspected: 2.5=<Z<3.5, Detected: Z>=3.5]\n";}
	}		
if(2.5<= $chr22_SCV_Check{Chr22} && $chr22_SCV_Check{Chr22} <3.5 || 2.5<= $chr22_SCV_Check{T22} && $chr22_SCV_Check{T22} <3.5)
	{
	if($MDS_Count<=1){print $OFILE4 "Trisomy22\tSUSPECTED\n";}
	$MDS_Count++;
	if(2.5<= $T_scv[0] && $T_scv[0] <3.5){print $OFILE4 "$T_band[0]\t$T_scv[0]\tSUSPECTED\t\t[Reference Range --> Emanuel Syndrome (Z-score): Normal: Z<2.5, Suspected: 2.5=<Z<3.5, Detected: Z>=3.5]\n";}
	if(2.5<= $T_scv[1] && $T_scv[1] <3.5){print $OFILE4 "$T_band[1]\t$T_scv[1]\tSUSPECTED\t\t[Reference Range --> Complete Chromosome 22 (Z-score): Normal: Z<2.5, Suspected: 2.5=<Z<3.5, Detected: Z>=3.5]\n";}
	}
if($chr22_SCV_Check{Chr22} <2.5 && $chr22_SCV_Check{T22} <2.5)
	{
	if($MDS_Count<=1){print $OFILE4 "Trisomy22\tNORMAL\n";}
	if($T_scv[0] <2.5){print $OFILE4 "$T_band[0]\t$T_scv[0]\tNORMAL\t\t[Reference Range --> Emanuel Syndrome (Z-score): Normal: Z<2.5, Suspected: 2.5=<Z<3.5, Detected: Z>=3.5]\n";}
	if($T_scv[1] <2.5){print $OFILE4 "$T_band[1]\t$T_scv[1]\tNORMAL\t\t[Reference Range --> Complete Chromosome 22 (Z-score): Normal: Z<2.5, Suspected: 2.5=<Z<3.5, Detected: Z>=3.5]\n";}
	}
print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################





##### Determine the Fetus SEX ###############################################################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line    
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);

	for ($columns[0] =~ m/MSY/)
	{
	chomp $columns[0];
	chomp $columns[4];
	
	if ($columns[4] >= -0.5)
		{
			print $OFILE4 "Sex:\tMale\n";
		}
		elsif ($columns[4] <= -0.8)
			{
			print $OFILE4 "Sex:\tFemale\n";
			}
			else
			{
			print $OFILE4 "Sex:\tUnDetermined\n";
			}
	}	
}
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################





##### Determine Female Sex Chromosomal Anueploidy ###########################################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open (INFile2, "$Sample_ID.SCV-NIPT_Result.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
my $SCV_X;
#
# go through the file line by line    
while (my $line2 = <INFile2>)  
{
	chomp ($line2);
	chop($line2) if ($line2 =~ m/\r$/);
	# split the current line on tabs
    my @columns2 = split(/\t/, $line2);
	
	for ($columns2[0] =~ m/Sex:/)
	{
	chomp $columns2[0];
	chomp $columns2[1];
	
	if ($columns2[1] =~ m/Female/)
		{
				# go through the file line by line    
				while (my $line = <INFile>)  
				{

				chomp ($line);
				chop($line) if ($line =~ m/\r$/);

				# split the current line on tabs
				my @columns = split(/\t/, $line);
				

						for ($columns[0] =~ m/ChrX/)
							{
							chomp $columns[0];
							chomp $columns[4];
							
							
							if ($columns[4] >= 1.6)
								{
									print $OFILE4 "Sex_Chromosomal_Anueploidy:\tXXX_DETECTED\t\t[Reference Range (Z-score): Normal: 0.3<Z<1.4, Xo-Suspected: 0.3=>Z>=0, Xo-Detected: Z<0, TrisomyX-Suspected: 1.6>Z>=1.4, TrisomyX-Detected: Z>=1.6]\n";
								}
							elsif ($columns[4] >= 1.4 && $columns[4] <= 1.6)
								{
									print $OFILE4 "Sex_Chromosomal_Anueploidy:\tXXX_SUSPECTED\t\t[Reference Range (Z-score): Normal: 0.3<Z<1.4, Xo-Suspected: 0.3=>Z>=0, Xo-Detected: Z<0, TrisomyX-Suspected: 1.6>Z>=1.4, TrisomyX-Detected: Z>=1.6]\n";
								}
							elsif ($columns[4] <= 0)
								{
									print $OFILE4 "Sex_Chromosomal_Anueploidy:\tXO_DETECTED\t\t[Reference Range (Z-score): Normal: 0.3<Z<1.4, Xo-Suspected: 0.3=>Z>=0, Xo-Detected: Z<0, TrisomyX-Suspected: 1.6>Z>=1.4, TrisomyX-Detected: Z>=1.6]\n";
								}
							elsif ($columns[4] >= 0 && $columns[4] <= 0.3)
								{
									print $OFILE4 "Sex_Chromosomal_Anueploidy:\tXO_SUSPECTED\t\t[Reference Range (Z-score): Normal: 0.3<Z<1.4, Xo-Suspected: 0.3=>Z>=0, Xo-Detected: Z<0, TrisomyX-Suspected: 1.6>Z>=1.4, TrisomyX-Detected: Z>=1.6]\n";
								}	
							else 	
								{
									print $OFILE4 "Sex_Chromosomal_Anueploidy:\tNORMAL\n";
								}	
							print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";	
							}
				}
				
		}
		
#### To save the SCV-X value for Male SCA determination. Do not delete below code lines.
		else
		{
				# go through the file line by line    
				while (my $line = <INFile>)  
				{

				chomp ($line);
				chop($line) if ($line =~ m/\r$/);

				# split the current line on tabs
				my @columns = split(/\t/, $line);
				

						for ($columns[0] =~ m/ChrX/)
							{
							chomp $columns[0];
							chomp $columns[4];
							
							$SCV_X = $columns[4];
							}
				}
		}
	}
}
#
close $OFILE4;
close INFile;
close INFile2;
#
#
#############################################################################################################





##### Determine the Male Sex Chromosomal Anueploidy #########################################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open (INFile2, "$Sample_ID.SCV-NIPT_Result.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line    
while (my $line2 = <INFile2>)  
{
	chomp ($line2);
	chop($line2) if ($line2 =~ m/\r$/);
	# split the current line on tabs
    my @columns2 = split(/\t/, $line2);
	
	
	for ($columns2[0] =~ m/Sex:/)
	{
	chomp $columns2[0];
	chomp $columns2[1];
	
		if ($columns2[1] =~ m/Male/)
		{
				# go through the file line by line    
				while (my $line = <INFile>)  
				{

				chomp ($line);
				chop($line) if ($line =~ m/\r$/);
				# split the current line on tabs
				my @columns = split(/\t/, $line);
				
				
						my $SCV_MSY;
						for ($columns[0] =~ m/MSY/)
						{
							chomp $columns[0];
							chomp $columns[4];
						
							$SCV_MSY = $columns[4];
							
							
							if($SCV_X > 0.8)
							{
								print $OFILE4 "Sex_Chromosomal_Anueploidy:\tXXY_DETECTED\t\t[Reference Range (Z-score): Multiple Chromosome X and Y values are used to determine the Sex Chromosome Anueploidy for Man. Please contact service provider if you need detailed scoring information]\n";
							}	
							elsif (abs($SCV_MSY)-abs($SCV_X) > 1.0)
							{
								print $OFILE4 "Sex_Chromosomal_Anueploidy:\tXYY_DETECTED\t\t[Reference Range (Z-score): Multiple Chromosome X and Y values are used to determine the Sex Chromosome Anueploidy for Man. Please contact service provider if you need detailed scoring information]\n";
							}
							else	
							{
								print $OFILE4 "Sex_Chromosomal_Anueploidy:\tNORMAL\n";
							}
						print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";
						}
				}
		}
	}
}
#	
close $OFILE4;
close INFile;
close INFile2;
#
#
#############################################################################################################





##### Can not Determine Sex Chromosomal Anueploidy ##########################################################
#
#
# open a filehandle to data.txt
open (INFile2, "$Sample_ID.SCV-NIPT_Result.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line    
while (my $line2 = <INFile2>)  
{
	chomp ($line2);
	chop($line2) if ($line2 =~ m/\r$/);
	# split the current line on tabs
    my @columns2 = split(/\t/, $line2);
	
	
	for ($columns2[0] =~ m/Sex:/)
	{
	chomp $columns2[0];
	chomp $columns2[1];
	
		if ($columns2[1] =~ m/UnDetermined/)
		{
			print $OFILE4 "Sex Chromosomal Anueploidy:\tUnDetermined\t\t[Attention : Please contact service provider or doctor for more information. Resequencing is be suggested to confirm the test Finding.]\n";
			print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";
		}
	}		
}
#	
close $OFILE4;
close INFile2;
#
#
#############################################################################################################





##### MD_1P36 MicroDeletion SCV Check ##########################################################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line
my %MD_1p36_SCV_Check;
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);
	
	chomp $columns[0];
	chomp $columns[4];
	for ($columns[0] =~ m/(MD_1p36DS|MD_1p36DS_1|MD_1p36DS_2|MD_1p36DS_3|MD_1p36DS_4)/)
	{
	$MD_1p36_SCV_Check{$columns[0]} = $columns[4];
	}	
}
@T_band = keys %MD_1p36_SCV_Check;
@T_scv = values %MD_1p36_SCV_Check;
my $MDS_Count = 1;
if($MD_1p36_SCV_Check{MD_1p36DS} <=-4 ||$MD_1p36_SCV_Check{MD_1p36DS_1} <=-4 || $MD_1p36_SCV_Check{MD_1p36DS_2} <=-4 || $MD_1p36_SCV_Check{MD_1p36DS_3} <=-4 || $MD_1p36_SCV_Check{MD_1p36DS_4} <=-4 )
	{
	print $OFILE4 "1p36_DeletionSyndrome\tDETECTED\n";
	$MDS_Count++;
	if($T_scv[0] <=-4){print $OFILE4 "1p36\t$T_scv[0]\tDETECTED\t\t[Reference Range --> Multiple 1p36DS associated cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[1] <=-4){print $OFILE4 "1p36.33\t$T_scv[1]\tDETECTED\t\t[Reference Range --> 1p36.33 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[2] <=-4){print $OFILE4 "1p36.32\t$T_scv[2]\tDETECTED\t\t[Reference Range --> 1p36.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[3] <=-4){print $OFILE4 "1p36.31\t$T_scv[3]\tDETECTED\t\t[Reference Range --> 1p36.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[4] <=-4){print $OFILE4 "1p36.23\t$T_scv[4]\tDETECTED\t\t[Reference Range --> 1p36.23 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}
if($MD_1p36_SCV_Check{MD_1p36DS} <=-3 && $MD_1p36_SCV_Check{MD_1p36DS} >-4 ||$MD_1p36_SCV_Check{MD_1p36DS_1} <=-3 && $MD_1p36_SCV_Check{MD_1p36DS_1} >-4 ||$MD_1p36_SCV_Check{MD_1p36DS_2} <=-3 && $MD_1p36_SCV_Check{MD_1p36DS_2} >-4 ||$MD_1p36_SCV_Check{MD_1p36DS_3} <=-3 && $MD_1p36_SCV_Check{MD_1p36DS_3} >-4 ||$MD_1p36_SCV_Check{MD_1p36DS_4} <=-3 && $MD_1p36_SCV_Check{MD_1p36DS_4} >-4 )
	{
	if($MDS_Count<=1){print $OFILE4 "1p36_DeletionSyndrome\tSUSPECTED\n";}
	$MDS_Count++;
	if($T_scv[0] <=-3 && $T_scv[0] >-4){print $OFILE4 "1p36\t$T_scv[0]\tSUSPECTED\t\t[Reference Range --> Multiple 1p36DS associated cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[1] <=-3 && $T_scv[1] >-4){print $OFILE4 "1p36.33\t$T_scv[1]\tSUSPECTED\t\t[Reference Range --> 1p36.33 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[2] <=-3 && $T_scv[2] >-4){print $OFILE4 "1p36.32\t$T_scv[2]\tSUSPECTED\t\t[Reference Range --> 1p36.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[3] <=-3 && $T_scv[3] >-4){print $OFILE4 "1p36.31\t$T_scv[3]\tSUSPECTED\t\t[Reference Range --> 1p36.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[4] <=-3 && $T_scv[4] >-4){print $OFILE4 "1p36.23\t$T_scv[4]\tSUSPECTED\t\t[Reference Range --> 1p36.23 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}
if($MD_1p36_SCV_Check{MD_1p36DS} >-3 ||$MD_1p36_SCV_Check{MD_1p36DS_1} >-3 || $MD_1p36_SCV_Check{MD_1p36DS_2} >-3 || $MD_1p36_SCV_Check{MD_1p36DS_3} >-3 || $MD_1p36_SCV_Check{MD_1p36DS_4} >-3 )
	{
	if($MDS_Count<=1){print $OFILE4 "1p36_DeletionSyndrome\tNORMAL\n";}
	if($T_scv[0] >-3){print $OFILE4 "1p36\t$T_scv[0]\tNORMAL\t\t[Reference Range --> Multiple 1p36DS associated cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[1] >-3){print $OFILE4 "1p36.33\t$T_scv[1]\tNORMAL\t\t[Reference Range --> 1p36.33 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[2] >-3){print $OFILE4 "1p36.32\t$T_scv[2]\tNORMAL\t\t[Reference Range --> 1p36.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[3] >-3){print $OFILE4 "1p36.31\t$T_scv[3]\tNORMAL\t\t[Reference Range --> 1p36.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[4] >-3){print $OFILE4 "1p36.23\t$T_scv[4]\tNORMAL\t\t[Reference Range --> 1p36.23 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}
	
print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################





##### MD_2q33DS MicroDeletion SCV Check ##########################################################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line
my %MD_2q33DS_SCV_Check;
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);
	
	chomp $columns[0];
	chomp $columns[4];
	for ($columns[0] =~ m/(MD_2q33DS|MD_2q33DS_1|MD_2q33DS_2|MD_2q33DS_3|MD_2q33DS_4|MD_2q33DS_5|MD_2q33DS_6|MD_2q33DS_7)/)
	{
	$MD_2q33DS_SCV_Check{$columns[0]} = $columns[4];
	}	
}
@T_band = keys %MD_2q33DS_SCV_Check;
@T_scv = values %MD_2q33DS_SCV_Check;
my $MDS_Count = 1;
if($MD_2q33DS_SCV_Check{MD_2q33DS} <=-5 || $MD_2q33DS_SCV_Check{MD_2q33DS_1} <=-5 || $MD_2q33DS_SCV_Check{MD_2q33DS_2} <=-4 || $MD_2q33DS_SCV_Check{MD_2q33DS_3} <=-4 || $MD_2q33DS_SCV_Check{MD_2q33DS_4} <=-4 || $MD_2q33DS_SCV_Check{MD_2q33DS_5} <=-4 || $MD_2q33DS_SCV_Check{MD_2q33DS_6} <=-4 || $MD_2q33DS_SCV_Check{MD_2q33DS_7} <=-4)
	{
	print $OFILE4 "2q33_DeletionSyndrome\tDETECTED\n";
	$MDS_Count++;
	if($T_scv[0] <=-5){print $OFILE4 "2q33\t$T_scv[0]\tDETECTED\t\t[Reference Range --> Multiple 2q33DS associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[1] <=-5){print $OFILE4 "2q31.1\t$T_scv[1]\tDETECTED\t\t[Reference Range --> 2q31.1 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[2] <=-4){print $OFILE4 "2q31.2\t$T_scv[2]\tDETECTED\t\t[Reference Range --> 2q31.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[3] <=-4){print $OFILE4 "2q31.3\t$T_scv[3]\tDETECTED\t\t[Reference Range --> 2q31.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[4] <=-4){print $OFILE4 "2q32.1\t$T_scv[4]\tDETECTED\t\t[Reference Range --> 2q32.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[5] <=-4){print $OFILE4 "2q32.2\t$T_scv[5]\tDETECTED\t\t[Reference Range --> 2q32.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[6] <=-4){print $OFILE4 "2q32.3\t$T_scv[6]\tDETECTED\t\t[Reference Range --> 2q32.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[7] <=-4){print $OFILE4 "2q33.1\t$T_scv[7]\tDETECTED\t\t[Reference Range --> 2q33.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}
if($MD_2q33DS_SCV_Check{MD_2q33DS} <=-4 && $MD_2q33DS_SCV_Check{MD_2q33DS} >-5 || $MD_2q33DS_SCV_Check{MD_2q33DS_1} <=-4 && $MD_2q33DS_SCV_Check{MD_2q33DS_1} >-5 || $MD_2q33DS_SCV_Check{MD_2q33DS_2} <=-3 && $MD_2q33DS_SCV_Check{MD_2q33DS_2} >-4 || $MD_2q33DS_SCV_Check{MD_2q33DS_3} <=-3 && $MD_2q33DS_SCV_Check{MD_2q33DS_3} >-4 || $MD_2q33DS_SCV_Check{MD_2q33DS_4} <=-3 && $MD_2q33DS_SCV_Check{MD_2q33DS_4} >-4 || $MD_2q33DS_SCV_Check{MD_2q33DS_5} <=-3 && $MD_2q33DS_SCV_Check{MD_2q33DS_5} >-4 || $MD_2q33DS_SCV_Check{MD_2q33DS_6} <=-3 && $MD_2q33DS_SCV_Check{MD_2q33DS_6} >-4 || $MD_2q33DS_SCV_Check{MD_2q33DS_7} <=-3 && $MD_2q33DS_SCV_Check{MD_2q33DS_7} >-4)
	{
	if($MDS_Count<=1){print $OFILE4 "2q33_DeletionSyndrome\tSUSPECTED\n";}
	$MDS_Count++;
	if($T_scv[0] <=-4 && $T_scv[0] >-5){print $OFILE4 "2q33\t$T_scv[0]\tSUSPECTED\t\t[Reference Range --> Multiple 2q33DS associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[1] <=-4 && $T_scv[1] >-5){print $OFILE4 "2q31.1\t$T_scv[1]\tSUSPECTED\t\t[Reference Range --> 2q31.1 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[2] <=-3 && $T_scv[2] >-4){print $OFILE4 "2q31.2\t$T_scv[2]\tSUSPECTED\t\t[Reference Range --> 2q31.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[3] <=-3 && $T_scv[3] >-4){print $OFILE4 "2q31.3\t$T_scv[3]\tSUSPECTED\t\t[Reference Range --> 2q31.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[4] <=-3 && $T_scv[4] >-4){print $OFILE4 "2q32.1\t$T_scv[4]\tSUSPECTED\t\t[Reference Range --> 2q32.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[5] <=-3 && $T_scv[5] >-4){print $OFILE4 "2q32.2\t$T_scv[5]\tSUSPECTED\t\t[Reference Range --> 2q32.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[6] <=-3 && $T_scv[6] >-4){print $OFILE4 "2q32.3\t$T_scv[6]\tSUSPECTED\t\t[Reference Range --> 2q32.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[7] <=-3 && $T_scv[7] >-4){print $OFILE4 "2q33.1\t$T_scv[7]\tSUSPECTED\t\t[Reference Range --> 2q33.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}
if($MD_2q33DS_SCV_Check{MD_2q33DS} >-4 || $MD_2q33DS_SCV_Check{MD_2q33DS_1} >-4 || $MD_2q33DS_SCV_Check{MD_2q33DS_2} >-3 || $MD_2q33DS_SCV_Check{MD_2q33DS_3} >-3 || $MD_2q33DS_SCV_Check{MD_2q33DS_4} >-3 || $MD_2q33DS_SCV_Check{MD_2q33DS_5} >-3 || $MD_2q33DS_SCV_Check{MD_2q33DS_6} >-3 || $MD_2q33DS_SCV_Check{MD_2q33DS_7} >-3)
	{
	if($MDS_Count<=1){print $OFILE4 "2q33_DeletionSyndrome\tNORMAL\n";}
	if($T_scv[0] >-4){print $OFILE4 "2q33\t$T_scv[0]\tNORMAL\t\t[Reference Range --> Multiple 2q33DS associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[1] >-4){print $OFILE4 "2q31.1\t$T_scv[1]\tNORMAL\t\t[Reference Range --> 2q31.1 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[2] >-3){print $OFILE4 "2q31.2\t$T_scv[2]\tNORMAL\t\t[Reference Range --> 2q31.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[3] >-3){print $OFILE4 "2q31.3\t$T_scv[3]\tNORMAL\t\t[Reference Range --> 2q31.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[4] >-3){print $OFILE4 "2q32.1\t$T_scv[4]\tNORMAL\t\t[Reference Range --> 2q32.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[5] >-3){print $OFILE4 "2q32.2\t$T_scv[5]\tNORMAL\t\t[Reference Range --> 2q32.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[6] >-3){print $OFILE4 "2q32.3\t$T_scv[6]\tNORMAL\t\t[Reference Range --> 2q32.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[7] >-3){print $OFILE4 "2q33.1\t$T_scv[7]\tNORMAL\t\t[Reference Range --> 2q33.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}
	
print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";	
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################





##### WolfHirschhorn_ MicroDeletion SCV Check ###############################################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line
my %MD_WHS_SCV_Check;
while (my $line = <INFile>)
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);
	
	chomp $columns[0];
	chomp $columns[4];
	for ($columns[0] =~ m/(WolfHirschhorn|WolfHirschhorn_1|WolfHirschhorn_2|WolfHirschhorn_3|WolfHirschhorn_4|WolfHirschhorn_5|WolfHirschhorn_6|WolfHirschhorn_7)/)
	{
	$MD_WHS_SCV_Check{$columns[0]} = $columns[4];
	}	
}
@T_band = keys %MD_WHS_SCV_Check;
@T_scv = values %MD_WHS_SCV_Check;
my $MDS_Count = 1;
if($MD_WHS_SCV_Check{WolfHirschhorn} <=-5 || $MD_WHS_SCV_Check{WolfHirschhorn_1} <=-3.5 || $MD_WHS_SCV_Check{WolfHirschhorn_2} <=-5 || $MD_WHS_SCV_Check{WolfHirschhorn_3} <=-8 || $MD_WHS_SCV_Check{WolfHirschhorn_4} <=-6 || $MD_WHS_SCV_Check{WolfHirschhorn_5} <=-4 || $MD_WHS_SCV_Check{WolfHirschhorn_6} <=-4 || $MD_WHS_SCV_Check{WolfHirschhorn_7} <=-4)
	{
	print $OFILE4 "WolfHirschhorn_Syndrome\tDETECTED\n";
	$MDS_Count++;
	if($T_scv[0] <=-5){print $OFILE4 "AllBand\t$T_scv[0]\tDETECTED\t\t[Reference Range --> Multiple WHS associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[1] <=-3.5){print $OFILE4 "4p16.3\t$T_scv[1]\tDETECTED\t\t[Reference Range --> 4p16.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-3.5, Detected: Z<=-3.5]\n";}
	if($T_scv[2] <=-5){print $OFILE4 "4p16.2\t$T_scv[2]\tDETECTED\t\t[Reference Range --> 4p16.2 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[3] <=-8){print $OFILE4 "4p16.1\t$T_scv[3]\tDETECTED\t\t[Reference Range --> 4p16.1 cytoband (Z-score): Normal: Z>-5, Suspected: -5.0>=<Z>-8.0, Detected: Z<=-8.0]\n";}
	if($T_scv[4] <=-6){print $OFILE4 "4p15.33\t$T_scv[4]\tDETECTED\t\t[Reference Range --> 4p15.33 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-6.0, Detected: Z<=-6.0]\n";}
	if($T_scv[5] <=-4){print $OFILE4 "4p15.32\t$T_scv[5]\tDETECTED\t\t[Reference Range --> 4p15.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[6] <=-4){print $OFILE4 "4p15.31\t$T_scv[6]\tDETECTED\t\t[Reference Range --> 4p15.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[7] <=-4){print $OFILE4 "4p15.2\t$T_scv[6]\tDETECTED\t\t[Reference Range --> 4p15.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}
if($MD_WHS_SCV_Check{WolfHirschhorn} <=-4 && $MD_WHS_SCV_Check{WolfHirschhorn} >-5 || $MD_WHS_SCV_Check{WolfHirschhorn_1} <=-3 && $MD_WHS_SCV_Check{WolfHirschhorn_1} >-3.5 || $MD_WHS_SCV_Check{WolfHirschhorn_2} <=-4 && $MD_WHS_SCV_Check{WolfHirschhorn_2} >-5 || $MD_WHS_SCV_Check{WolfHirschhorn_3} <=-5 && $MD_WHS_SCV_Check{WolfHirschhorn_3} >-8 || $MD_WHS_SCV_Check{WolfHirschhorn_4} <=-4 && $MD_WHS_SCV_Check{WolfHirschhorn_4} >-6 || $MD_WHS_SCV_Check{WolfHirschhorn_5} <=-3 && $MD_WHS_SCV_Check{WolfHirschhorn_5} >-4 || $MD_WHS_SCV_Check{WolfHirschhorn_6} <=-3 && $MD_WHS_SCV_Check{WolfHirschhorn_6} >-4 || $MD_WHS_SCV_Check{WolfHirschhorn_7} <=-3 && $MD_WHS_SCV_Check{WolfHirschhorn_7} >-4)
	{
	if($MDS_Count<=1){print $OFILE4 "WolfHirschhorn_Syndrome\tSUSPECTED\n";}
	$MDS_Count++;
	if($T_scv[0] <=-4 && $T_scv[0] >-5){print $OFILE4 "AllBand\t$T_scv[0]tSUSPECTED\t\t[Reference Range --> Multiple WHS associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[1] <=-3 && $T_scv[1] >-3.5){print $OFILE4 "4p16.3\t$T_scv[1]tSUSPECTED\t\t[Reference Range --> 4p16.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-3.5, Detected: Z<=-3.5]\n";}
	if($T_scv[2] <=-4 && $T_scv[2] >-5){print $OFILE4 "4p16.2\t$T_scv[2]tSUSPECTED\t\t[Reference Range --> 4p16.2 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[3] <=-5 && $T_scv[3] >-8){print $OFILE4 "4p16.1\t$T_scv[3]tSUSPECTED\t\t[Reference Range --> 4p16.1 cytoband (Z-score): Normal: Z>-5, Suspected: -5.0>=<Z>-8.0, Detected: Z<=-8.0]\n";}
	if($T_scv[4] <=-4 && $T_scv[4] >-6){print $OFILE4 "4p15.33\t$T_scv[4]tSUSPECTED\t\t[Reference Range --> 4p15.33 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-6.0, Detected: Z<=-6.0]\n";}
	if($T_scv[5] <=-3 && $T_scv[5] >-4){print $OFILE4 "4p15.32\t$T_scv[5]tSUSPECTED\t\t[Reference Range --> 4p15.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[6] <=-3 && $T_scv[6] >-4){print $OFILE4 "4p15.31\t$T_scv[6]tSUSPECTED\t\t[Reference Range --> 4p15.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[7] <=-3 && $T_scv[7] >-4){print $OFILE4 "4p15.2\t$T_scv[7]tSUSPECTED\t\t[Reference Range --> 4p15.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}
if($MD_WHS_SCV_Check{WolfHirschhorn} >-4 || $MD_WHS_SCV_Check{WolfHirschhorn_1} >-3 || $MD_WHS_SCV_Check{WolfHirschhorn_2} >-4 || $MD_WHS_SCV_Check{WolfHirschhorn_3} >-5 || $MD_WHS_SCV_Check{WolfHirschhorn_4} >-4 || $MD_WHS_SCV_Check{WolfHirschhorn_5} >-3 || $MD_WHS_SCV_Check{WolfHirschhorn_6} >-3 || $MD_WHS_SCV_Check{WolfHirschhorn_7} >-3)
	{
	if($MDS_Count<=1){print $OFILE4 "WolfHirschhorn_Syndrome\tNORMAL\n";}
	if($T_scv[0] >-4){print $OFILE4 "AllBand\t$T_scv[0]\tNORMAL\t\t[Reference Range --> Multiple WHS associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[1] >-3){print $OFILE4 "4p16.3\t$T_scv[1]\tNORMAL\t\t[Reference Range --> 4p16.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-3.5, Detected: Z<=-3.5]\n";}
	if($T_scv[2] >-4){print $OFILE4 "4p16.2\t$T_scv[2]\tNORMAL\t\t[Reference Range --> 4p16.2 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[3] >-5){print $OFILE4 "4p16.1\t$T_scv[3]\tNORMAL\t\t[Reference Range --> 4p16.1 cytoband (Z-score): Normal: Z>-5, Suspected: -5.0>=<Z>-8.0, Detected: Z<=-8.0]\n";}
	if($T_scv[4] >-4){print $OFILE4 "4p15.33\t$T_scv[4]\tNORMAL\t\t[Reference Range --> 4p15.33 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-6.0, Detected: Z<=-6.0]\n";}
	if($T_scv[5] >-3){print $OFILE4 "4p15.32\t$T_scv[5]\tNORMAL\t\t[Reference Range --> 4p15.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[6] >-3){print $OFILE4 "4p15.31\t$T_scv[6]\tNORMAL\t\t[Reference Range --> 4p15.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[7] >-3){print $OFILE4 "4p15.2\t$T_scv[7]\tNORMAL\t\t[Reference Range --> 4p15.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}
	
print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";	
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################





##### MD_CriDuChatDS MicroDeletion SCV Check ##########################################################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line
my %MD_CriDuChat_SCV_Check;
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);
	
	chomp $columns[0];
	chomp $columns[4];
	for ($columns[0] =~ m/(CriDuChat|CriDuChat_1|CriDuChat_2|CriDuChat_3|CriDuChat_4|CriDuChat_5|CriDuChat_6|CriDuChat_7|CriDuChat_8|CriDuChat_9)/)
	{
	$MD_CriDuChat_SCV_Check{$columns[0]} = $columns[4];
	}	
}
@T_band = keys %MD_CriDuChat_SCV_Check;
@T_scv = values %MD_CriDuChat_SCV_Check;
my $MDS_Count = 1;
if($MD_CriDuChat_SCV_Check{CriDuChat} <=-6 ||$MD_CriDuChat_SCV_Check{CriDuChat_1} <=-4 || $MD_CriDuChat_SCV_Check{CriDuChat_2} <=-4 || $MD_CriDuChat_SCV_Check{CriDuChat_3} <=-4 || $MD_CriDuChat_SCV_Check{CriDuChat_4} <=-5 || $MD_CriDuChat_SCV_Check{CriDuChat_5} <=-5 || $MD_CriDuChat_SCV_Check{CriDuChat_6} <=-4 || $MD_CriDuChat_SCV_Check{CriDuChat_7} <=-4 || $MD_CriDuChat_SCV_Check{CriDuChat_8} <=-4 || $MD_CriDuChat_SCV_Check{CriDuChat_9} <=-4)
	{
	print $OFILE4 "Cri-Du-Chat_Syndrome\tDETECTED\n";
	$MDS_Count++;
	if($T_scv[0] <=-6){print $OFILE4 "CDC_AllBand\t$T_scv[0]\tDETECTED\t\t[Reference Range --> Multiple CDC associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-6.0, Detected: Z<=-6.0]\n";}
	if($T_scv[1] <=-4){print $OFILE4 "5p15.33\t$T_scv[1]\tDETECTED\t\t[Reference Range --> 5p15.33 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[2] <=-4){print $OFILE4 "5p15.32\t$T_scv[2]\tDETECTED\t\t[Reference Range --> 5p15.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[3] <=-4){print $OFILE4 "5p15.31\t$T_scv[3]\tDETECTED\t\t[Reference Range --> 5p15.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[4] <=-5){print $OFILE4 "5p15.2\t$T_scv[4]\tDETECTED\t\t[Reference Range --> 5p15.2 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[5] <=-5){print $OFILE4 "5p15.1\t$T_scv[5]\tDETECTED\t\t[Reference Range --> 5p15.1 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[6] <=-4){print $OFILE4 "5p14.3\t$T_scv[6]\tDETECTED\t\t[Reference Range --> 5p14.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[7] <=-4){print $OFILE4 "5p14.2\t$T_scv[7]\tDETECTED\t\t[Reference Range --> 5p14.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[8] <=-4){print $OFILE4 "5p14.1\t$T_scv[8]\tDETECTED\t\t[Reference Range --> 5p14.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[9] <=-4){print $OFILE4 "5p13.3\t$T_scv[9]\tDETECTED\t\t[Reference Range --> 5p13.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}
if($MD_CriDuChat_SCV_Check{CriDuChat} <=-4 && $MD_CriDuChat_SCV_Check{CriDuChat} >-6 || $MD_CriDuChat_SCV_Check{CriDuChat_1} <=-3 && $MD_CriDuChat_SCV_Check{CriDuChat_1} >-4 || $MD_CriDuChat_SCV_Check{CriDuChat_2} <=-3 && $MD_CriDuChat_SCV_Check{CriDuChat_2} >-4 || $MD_CriDuChat_SCV_Check{CriDuChat_3} <=-3 && $MD_CriDuChat_SCV_Check{CriDuChat_3} >-4 || $MD_CriDuChat_SCV_Check{CriDuChat_4} <=-4 && $MD_CriDuChat_SCV_Check{CriDuChat_4} >-5 || $MD_CriDuChat_SCV_Check{CriDuChat_5} <=-4 && $MD_CriDuChat_SCV_Check{CriDuChat_5} >-5 || $MD_CriDuChat_SCV_Check{CriDuChat_6} <=-3 && $MD_CriDuChat_SCV_Check{CriDuChat_6} >-4 || $MD_CriDuChat_SCV_Check{CriDuChat_7} <=-3 && $MD_CriDuChat_SCV_Check{CriDuChat_7} >-4|| $MD_CriDuChat_SCV_Check{CriDuChat_8} <=-3 && $MD_CriDuChat_SCV_Check{CriDuChat_8} >-4|| $MD_CriDuChat_SCV_Check{CriDuChat_9} <=-3 && $MD_CriDuChat_SCV_Check{CriDuChat_9} >-4)
	{
	if($MDS_Count<=1){print $OFILE4 "Cri-Du-Chat_Syndrome\tSUSPECTED\n";}
	$MDS_Count++;
	if($T_scv[0] <=-4 && $T_scv[0] >-6){print $OFILE4 "CDC_AllBand\t$T_scv[0]\tSUSPECTED\t\t[Reference Range --> Multiple CDC associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-6.0, Detected: Z<=-6.0]\n";}
	if($T_scv[1] <=-3 && $T_scv[1] >-4){print $OFILE4 "5p15.33\t$T_scv[1]\tSUSPECTED\t\t[Reference Range --> 5p15.33 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[2] <=-3 && $T_scv[2] >-4){print $OFILE4 "5p15.32\t$T_scv[2]\tSUSPECTED\t\t[Reference Range --> 5p15.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[3] <=-3 && $T_scv[3] >-4){print $OFILE4 "5p15.31\t$T_scv[3]\tSUSPECTED\t\t[Reference Range --> 5p15.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[4] <=-4 && $T_scv[4] >-5){print $OFILE4 "5p15.2\t$T_scv[4]\tSUSPECTED\t\t[Reference Range --> 5p15.2 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[5] <=-4 && $T_scv[5] >-5){print $OFILE4 "5p15.1\t$T_scv[5]\tSUSPECTED\t\t[Reference Range --> 5p15.1 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[6] <=-3 && $T_scv[6] >-4){print $OFILE4 "5p14.3\t$T_scv[6]\tSUSPECTED\t\t[Reference Range --> 5p14.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[7] <=-3 && $T_scv[7] >-4){print $OFILE4 "5p14.2\t$T_scv[7]\tSUSPECTED\t\t[Reference Range --> 5p14.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[8] <=-3 && $T_scv[8] >-4){print $OFILE4 "5p14.1\t$T_scv[8]\tSUSPECTED\t\t[Reference Range --> 5p14.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[9] <=-3 && $T_scv[9] >-4){print $OFILE4 "5p13.3\t$T_scv[9]\tSUSPECTED\t\t[Reference Range --> 5p13.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}
if($MD_CriDuChat_SCV_Check{CriDuChat} >-4 ||$MD_CriDuChat_SCV_Check{CriDuChat_1} >-3 || $MD_CriDuChat_SCV_Check{CriDuChat_2} >-3 || $MD_CriDuChat_SCV_Check{CriDuChat_3} >-3 || $MD_CriDuChat_SCV_Check{CriDuChat_4} >-4 || $MD_CriDuChat_SCV_Check{CriDuChat_5} >-4 || $MD_CriDuChat_SCV_Check{CriDuChat_6} >-3 || $MD_CriDuChat_SCV_Check{CriDuChat_7} >-3 || $MD_CriDuChat_SCV_Check{CriDuChat_8} >-3 || $MD_CriDuChat_SCV_Check{CriDuChat_9} >-3)
	{
	if($MDS_Count<=1){print $OFILE4 "Cri-Du-Chat_Syndrome\tNORMAL\n";}
	if($T_scv[0] >-4){print $OFILE4 "AllBand\t$T_scv[0]\tNORMAL\t\t[Reference Range --> Multiple CDC associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-6.0, Detected: Z<=-6.0]\n";}
	if($T_scv[1] >-3){print $OFILE4 "5p15.33\t$T_scv[1]\tNORMAL\t\t[Reference Range --> 5p15.33 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[2] >-3){print $OFILE4 "5p15.32\t$T_scv[2]\tNORMAL\t\t[Reference Range --> 5p15.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[3] >-3){print $OFILE4 "5p15.31\t$T_scv[3]\tNORMAL\t\t[Reference Range --> 5p15.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[4] >-4){print $OFILE4 "5p15.2\t$T_scv[4]\tNORMAL\t\t[Reference Range --> 5p15.2 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[5] >-4){print $OFILE4 "5p15.1\t$T_scv[5]\tNORMAL\t\t[Reference Range --> 5p15.1 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]\n";}
	if($T_scv[6] >-3){print $OFILE4 "5p14.3\t$T_scv[6]\tNORMAL\t\t[Reference Range --> 5p14.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[7] >-3){print $OFILE4 "5p14.2\t$T_scv[7]\tNORMAL\t\t[Reference Range --> 5p14.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[8] >-3){print $OFILE4 "5p14.1\t$T_scv[8]\tNORMAL\t\t[Reference Range --> 5p14.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[9] >-3){print $OFILE4 "5p13.3\t$T_scv[9]\tNORMAL\t\t[Reference Range --> 5p13.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}
	
print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";	
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################





##### MD_WilliamsBeuren Syndrome SCV Check ##########################################################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line
my %MD_WBS_SCV_Check;
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);
	
	chomp $columns[0];
	chomp $columns[4];
	##for ($columns[0] =~ m/(WilliamsBeuren|WilliamsBeuren_1|WilliamsBeuren_2)/)
	for ($columns[0] =~ m/(WilliamsBeuren_1)/)
	{
	$MD_WBS_SCV_Check{$columns[0]} = $columns[4];
	}	
}
@T_band = keys %MD_WBS_SCV_Check;
@T_scv = values %MD_WBS_SCV_Check;
my $MDS_Count = 1;
if($MD_WBS_SCV_Check{WilliamsBeuren_1} <=-2.5)
	{
	print $OFILE4 "WilliamsBeuren_Syndrome\tDETECTED\n";
	$MDS_Count++;
	if($T_scv[0] <=-2.5){print $OFILE4 "7q11.23\t$T_scv[0]\tDETECTED\t\t[Reference Range --> 7q11.23 cytoband (Z-score): Normal: Z>-2, Suspected: -2.0>=<Z>-2.5, Detected: Z<=-2.5]\n";}
	}
if($MD_WBS_SCV_Check{WilliamsBeuren_1} <=-2 && $MD_WBS_SCV_Check{WilliamsBeuren_1} >-2.5)
	{
	if($MDS_Count<=1){print $OFILE4 "WilliamsBeuren_Syndrome\tSUSPECTED\n";}
	$MDS_Count++;
	if($T_scv[0] <=-2 && $T_scv[1] >-2.5){print $OFILE4 "7q11.23\t$T_scv[0]\tSUSPECTED\t\t[Reference Range --> 7q11.23 cytoband (Z-score): Normal: Z>-2, Suspected: -2.0>=<Z>-2.5, Detected: Z<=-2.5]\n";}
	}
if($MD_WBS_SCV_Check{WilliamsBeuren_1} >-2)
	{
	if($MDS_Count<=1){print $OFILE4 "WilliamsBeuren_Syndrome\tNORMAL\n";}
	if($T_scv[0] >-2){print $OFILE4 "7q11.23\t$T_scv[0]\tNORMAL\t\t[Reference Range --> 7q11.23 cytoband (Z-score): Normal: Z>-2, Suspected: -2.0>=<Z>-2.5, Detected: Z<=-2.5]\n";}
	}
	
print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";	
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################





##### Jacobsen Syndrome MicroDeletion SCV Check ##########################################################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line
my %MD_Jacobsen_SCV_Check;
while (my $line = <INFile>)
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);
	
	chomp $columns[0];
	chomp $columns[4];
	for ($columns[0] =~ m/(Jacobsen|Jacobsen_4|Jacobsen_5|Jacobsen_6|Jacobsen_7)/)
	{
	$MD_Jacobsen_SCV_Check{$columns[0]} = $columns[4];
	}	
}
@T_band = keys %MD_Jacobsen_SCV_Check;
@T_scv = values %MD_Jacobsen_SCV_Check;
my $MDS_Count = 1;
if($MD_Jacobsen_SCV_Check{Jacobsen} <=-4 || $MD_Jacobsen_SCV_Check{Jacobsen_4} <=-4 || $MD_Jacobsen_SCV_Check{Jacobsen_5} <=-4 || $MD_Jacobsen_SCV_Check{Jacobsen_6} >=8 || $MD_Jacobsen_SCV_Check{Jacobsen_7} <=-4)
	{
	print $OFILE4 "Jacobsen_Syndrome\tDETECTED\n";
	$MDS_Count++;
	if($T_scv[0] <=-4){print $OFILE4 "AllBand\t$T_scv[0]\tDETECTED\t\t[Reference Range --> Multiple Jacobsen_Syndrome associated cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[1] <=-4){print $OFILE4 "11q24.1\t$T_scv[1]\tDETECTED\t\t[Reference Range --> 11q24.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[2] <=-4){print $OFILE4 "11q24.2\t$T_scv[2]\tDETECTED\t\t[Reference Range --> 11q24.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[3] <=-4){print $OFILE4 "11q24.3\t$T_scv[3]\tDETECTED\t\t[Reference Range --> 11q24.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[4] <=-4){print $OFILE4 "11q25\t$T_scv[4]\tDETECTED\t\t[Reference Range --> 11q25 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}
if($MD_Jacobsen_SCV_Check{Jacobsen} <=-3 && $MD_Jacobsen_SCV_Check{Jacobsen} >-4|| $MD_Jacobsen_SCV_Check{Jacobsen_4} <=-3 && $MD_Jacobsen_SCV_Check{Jacobsen_4} >-4|| $MD_Jacobsen_SCV_Check{Jacobsen_5} <=-3 && $MD_Jacobsen_SCV_Check{Jacobsen_5} >-4|| $MD_Jacobsen_SCV_Check{Jacobsen_6} <=-3 && $MD_Jacobsen_SCV_Check{Jacobsen_6} >-4|| $MD_Jacobsen_SCV_Check{Jacobsen_7} <=-3 && $MD_Jacobsen_SCV_Check{Jacobsen_7} >-4)
	{
	if($MDS_Count<=1){print $OFILE4 "Jacobsen_Syndrome\tSUSPECTED\n";}
	$MDS_Count++;
	if($T_scv[0] <=-3 && $T_scv[0] >-4){print $OFILE4 "AllBand\t$T_scv[0]\tSUSPECTED\t\t[Reference Range --> Multiple Jacobsen_Syndrome associated cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[1] <=-3 && $T_scv[1] >-4){print $OFILE4 "11q24.1\t$T_scv[1]\tSUSPECTED\t\t[Reference Range --> 11q24.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[2] <=-3 && $T_scv[2] >-4){print $OFILE4 "11q24.2\t$T_scv[2]\tSUSPECTED\t\t[Reference Range --> 11q24.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[3] <=-3 && $T_scv[3] >-4){print $OFILE4 "11q24.3\t$T_scv[3]\tSUSPECTED\t\t[Reference Range --> 11q24.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[4] <=-3 && $T_scv[4] >-4){print $OFILE4 "11q25\t$T_scv[4]\tSUSPECTED\t\t[Reference Range --> 11q25 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}
if($MD_Jacobsen_SCV_Check{Jacobsen} >-3 || $MD_Jacobsen_SCV_Check{Jacobsen_4} >-3 || $MD_Jacobsen_SCV_Check{Jacobsen_5} >-3 || $MD_Jacobsen_SCV_Check{Jacobsen_6} >-3 || $MD_Jacobsen_SCV_Check{Jacobsen_7} >-3)
	{
	if($MDS_Count<=1){print $OFILE4 "Jacobsen_Syndrome\tNORMAL\n";}
	if($T_scv[0] >-3){print $OFILE4 "AllBand\t$T_scv[0]\tNORMAL\t\t[Reference Range --> Multiple Jacobsen_Syndrome associated cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[1] >-3){print $OFILE4 "11q24.1\t$T_scv[1]\tNORMAL\t\t[Reference Range --> 11q24.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[2] >-3){print $OFILE4 "11q24.2\t$T_scv[2]\tNORMAL\t\t[Reference Range --> 11q24.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[3] >-3){print $OFILE4 "11q24.3\t$T_scv[3]\tNORMAL\t\t[Reference Range --> 11q24.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	if($T_scv[4] >-3){print $OFILE4 "11q25\t$T_scv[4]\tNORMAL\t\t[Reference Range --> 11q25 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]\n";}
	}

print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";	
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################





##### MD_PraderWilli/Angelmen Syndrome SCV Check ############################################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line
my %MD_PWS_SCV_Check;
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);
	
	chomp $columns[0];
	chomp $columns[4];
	for ($columns[0] =~ m/(PraderWilli|PraderWilli_2|PraderWilli_3)/)
	{
	$MD_PWS_SCV_Check{$columns[0]} = $columns[4];
	}
}
@T_band = keys %MD_PWS_SCV_Check;
@T_scv = values %MD_PWS_SCV_Check;
my $MDS_Count = 1;
if($MD_PWS_SCV_Check{PraderWilli} <=-3.5 || $MD_PWS_SCV_Check{PraderWilli_2} <=-3.5 || $MD_PWS_SCV_Check{PraderWilli_3} <=-4)
	{
	print $OFILE4 "PraderWilli/Angelmen_Syndrome\tDETECTED\n";
	$MDS_Count++;
	if($T_scv[0] <=-3.5){print $OFILE4 "AllBand\t$T_scv[0]\tDETECTED\t\t[Reference Range --> Multiple PraderWilli/Angelmen_Syndrome associated cytoband (Z-score): Normal: Z>-2.5, Suspected: -2.5>=<Z>-3.5, Detected: Z<=-3.5]\n";}
	if($T_scv[1] <=-3.5){print $OFILE4 "15q11.2\t$T_scv[1]\tDETECTED\t\t[Reference Range --> 15q11.2 cytoband (Z-score): Normal: Z>-2.5, Suspected: -2.5>=<Z>-3.5, Detected: Z<=-3.5]\n";}
	if($T_scv[2] <=-4){print $OFILE4 "15q12\t$T_scv[2]\tDETECTED\t\t[Reference Range --> 15q12 cytoband (Z-score): Normal: Z>-3, Suspected: -3>=<Z>-4, Detected: Z<=-4]\n";}
	}
if($MD_PWS_SCV_Check{PraderWilli} <=-2.5 && $MD_PWS_SCV_Check{PraderWilli} >-3.5|| $MD_PWS_SCV_Check{PraderWilli_2} <=-2.5 && $MD_PWS_SCV_Check{PraderWilli_2} >-3.5|| $MD_PWS_SCV_Check{PraderWilli_3} <=-3 && $MD_PWS_SCV_Check{PraderWilli_3} >-4)
	{
	if($MDS_Count<=1){print $OFILE4 "PraderWilli/Angelmen_Syndrome\tSUSPECTED\n";}
	$MDS_Count++;
	if($T_scv[0] <=-2.5 && $T_scv[0] >-3.5){print $OFILE4 "AllBand\t$T_scv[0]\tSUSPECTED\t\t[Reference Range --> Multiple PraderWilli/Angelmen_Syndrome associated cytoband (Z-score): Normal: Z>-2.5, Suspected: -2.5>=<Z>-3.5, Detected: Z<=-3.5]\n";}
	if($T_scv[1] <=-2.5 && $T_scv[1] >-3.5){print $OFILE4 "15q11.2\t$T_scv[1]\tSUSPECTED\t\t[Reference Range --> 15q11.2 cytoband (Z-score): Normal: Z>-2.5, Suspected: -2.5>=<Z>-3.5, Detected: Z<=-3.5]\n";}
	if($T_scv[2] <=-3 && $T_scv[2] >-4){print $OFILE4 "15q12\t$T_scv[2]\tSUSPECTED\t\t[Reference Range --> 15q12 cytoband (Z-score): Normal: Z>-3, Suspected: -3>=<Z>-4, Detected: Z<=-4]\n";}
	}
if($MD_PWS_SCV_Check{PraderWilli} >-2.5 || $MD_PWS_SCV_Check{PraderWilli_2} >-2.5 || $MD_PWS_SCV_Check{PraderWilli_3} >-3)
	{
	if($MDS_Count<=1){print $OFILE4 "PraderWilli/Angelmen_Syndrome\tNORMAL\n";}
	if($T_scv[0] >-2.5){print $OFILE4 "AllBand\t$T_scv[0]\tNORMAL\t\t[Reference Range --> Multiple PraderWilli/Angelmen_Syndrome associated cytoband (Z-score): Normal: Z>-2.5, Suspected: -2.5>=<Z>-3.5, Detected: Z<=-3.5]\n";}
	if($T_scv[1] >-2.5){print $OFILE4 "15q11.2\t$T_scv[1]\tNORMAL\t\t[Reference Range --> 15q11.2 cytoband (Z-score): Normal: Z>-2.5, Suspected: -2.5>=<Z>-3.5, Detected: Z<=-3.5]\n";}
	if($T_scv[2] >-3){print $OFILE4 "15q12\t$T_scv[2]\tNORMAL\t\t[Reference Range --> 15q12 cytoband (Z-score): Normal: Z>-3, Suspected: -3>=<Z>-4, Detected: Z<=-4]\n";}
	}
	
print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n\n";	
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################





##### MD_DiGeorge Syndrome SCV Check ##########################################################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.CytoBand_SCV.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Result.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line
my %MD_DGS_SCV_Check;
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);
	
	chomp $columns[0];
	chomp $columns[4];
	for ($columns[0] =~ m/(DiGeorge_1)/)
	{
	$MD_DGS_SCV_Check{$columns[0]} = $columns[4];
	}
}
@T_band = keys %MD_DGS_SCV_Check;
@T_scv = values %MD_DGS_SCV_Check;
my $MDS_Count = 1;
if($MD_DGS_SCV_Check{DiGeorge_1} <=-4)
	{
	print $OFILE4 "DiGeorge_Syndrome\tDETECTED\n";
	$MDS_Count++;
	if($T_scv[0] <=-4){print $OFILE4 "22q11.21\t$T_scv[0]\tDETECTED\t\t[Reference Range --> 22q11.21 cytoband (Z-score): Normal: Z>-3, Suspected: -3>=<Z>-4, Detected: Z<=-4]\n";}
	}
if($MD_DGS_SCV_Check{DiGeorge_1} <=-3 && $MD_DGS_SCV_Check{DiGeorge_1} >-4)
	{
	if($MDS_Count<=1){print $OFILE4 "DiGeorge_Syndrome\tSUSPECTED\n";}
	$MDS_Count++;
	if($T_scv[0] <=-3 && $T_scv[0] >-4){print $OFILE4 "22q11.21\t$T_scv[0]\tSUSPECTED\t\t[Reference Range --> 22q11.21 cytoband (Z-score): Normal: Z>-3, Suspected: -3>=<Z>-4, Detected: Z<=-4]\n";}
	}
if($MD_DGS_SCV_Check{DiGeorge_1} >-3)
	{
	if($MDS_Count<=1){print $OFILE4 "DiGeorge_Syndrome\tNORMAL\n";}
	print $OFILE4 "22q11.21\t$T_scv[0]\tNORMAL\t\t[Reference Range --> 22q11.21 cytoband (Z-score): Normal: Z>-3, Suspected: -3>=<Z>-4, Detected: Z<=-4]\n";
	}
	
print $OFILE4 "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";	
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################





##### Create Summary Report #################################################################################
#
#
# open a filehandle to data.txt
open (INFile, "$Sample_ID.SCV-NIPT_Result.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Summary.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line    
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);

	for ($columns[0] =~ m/(Trisomy9|Trisomy13|Trisomy16|Trisomy18|Trisomy21|Trisomy22|Sex:|Sex_Chromosomal_Anueploidy:)/)
	{
	chomp $columns[0];
	chomp $columns[1];
	print $OFILE4 "$columns[0]\t$columns[1]\n";
	}	
}
print $OFILE4 "------------------------------------------------------------\n\n";
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
open (INFile, "$Sample_ID.SCV-NIPT_Result.txt") or die;
open my $OFILE4, '>>', $Sample_ID.'.SCV-NIPT_Summary.txt' or die "Cannot create file for output: $!";
#
#
# go through the file line by line
while (my $line = <INFile>)  
{
	chomp ($line);
	chop($line) if ($line =~ m/\r$/);
	# split the current line on tabs
    my @columns = split(/\t/, $line);

	for ($columns[0] =~ m/(1p36_DeletionSyndrome|2q33_DeletionSyndrome|WolfHirschhorn_Syndrome|Cri-Du-Chat_Syndrome|WilliamsBeuren_Syndrome|Jacobsen_Syndrome|PraderWilli\/Angelmen_Syndrome|DiGeorge_Syndrome)/)
	{
	chomp $columns[0];
	chomp $columns[1];
	print $OFILE4 "$columns[0]\t$columns[1]\n";
	}	
}
#
# close the filehandle and exit
#
close $OFILE4;
close INFile;
#
#
#############################################################################################################