Trisomy9	NORMAL
Trisomy13	NORMAL
Trisomy16	NORMAL
Trisomy18	NORMAL
Trisomy21	NORMAL
Trisomy22	NORMAL
Sex:	Male
Sex_Chromosomal_Anueploidy:	NORMAL
------------------------------------------------------------

1p36_DeletionSyndrome	NORMAL
2q33_DeletionSyndrome	NORMAL
WolfHirschhorn_Syndrome	DETECTED
Cri-Du-Chat_Syndrome	NORMAL
WilliamsBeuren_Syndrome	NORMAL
Jacobsen_Syndrome	NORMAL
PraderWilli/Angelmen_Syndrome	NORMAL
DiGeorge_Syndrome	NORMAL
