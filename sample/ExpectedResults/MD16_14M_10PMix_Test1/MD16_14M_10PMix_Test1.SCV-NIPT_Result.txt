Trisomy9	NORMAL
T9p	0.343253975905638	NORMAL		[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Trisomy13	NORMAL
Chr13	0.721250658046733	NORMAL		[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Trisomy16	NORMAL
Chr16	0.419168554404294	NORMAL		[Reference Range (Z-score): Normal: Z<3.5, Suspected: 3.5=<Z<4, Detected: Z>=4]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Trisomy18	NORMAL
Chr18	-0.381215230100237	NORMAL		[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Trisomy21	NORMAL
Chr21	-0.657310943528906	NORMAL		[Reference Range --> Complete Chromosome 21 (Z-score): Normal: Z<4, Suspected: 4=<Z<5, Detected: Z>=5]
T21q	-0.478013722878562	NORMAL		[Reference Range --> T21q - q-arm of Chromosome 21 (Z-score): Normal: Z<4, Suspected: 4=<Z<5, Detected: Z>=5]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Trisomy22	NORMAL
T22	-0.134863652828801	NORMAL		[Reference Range --> Emanuel Syndrome (Z-score): Normal: Z<2.5, Suspected: 2.5=<Z<3.5, Detected: Z>=3.5]
Chr22	0.122457589149684	NORMAL		[Reference Range --> Complete Chromosome 22 (Z-score): Normal: Z<2.5, Suspected: 2.5=<Z<3.5, Detected: Z>=3.5]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Sex:	Female
Sex_Chromosomal_Anueploidy:	NORMAL
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

1p36_DeletionSyndrome	NORMAL
1p36	-0.976262181286308	NORMAL		[Reference Range --> Multiple 1p36DS associated cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
1p36.33	0.418617043630899	NORMAL		[Reference Range --> 1p36.33 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
1p36.32	-1.44738289904947	NORMAL		[Reference Range --> 1p36.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
1p36.31	-1.04137398433726	NORMAL		[Reference Range --> 1p36.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
1p36.23	-0.563189728296942	NORMAL		[Reference Range --> 1p36.23 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

2q33_DeletionSyndrome	DETECTED
2q33	-6.79740150472168	DETECTED		[Reference Range --> Multiple 2q33DS associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]
2q32.1	-9.46316502731572	DETECTED		[Reference Range --> 2q32.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
2q32.2	-7.1718661373363	DETECTED		[Reference Range --> 2q32.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
2q33.1	-17.9498911539738	DETECTED		[Reference Range --> 2q33.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
2q31.1	-0.316943790945041	NORMAL		[Reference Range --> 2q31.1 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]
2q31.2	-2.98649118773093	NORMAL		[Reference Range --> 2q31.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
2q31.3	0.626420585567367	NORMAL		[Reference Range --> 2q31.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
2q32.3	0.149566675370099	NORMAL		[Reference Range --> 2q32.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

WolfHirschhorn_Syndrome	NORMAL
AllBand	-1.66165836355004	NORMAL		[Reference Range --> Multiple WHS associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]
4p16.3	0.999728438309852	NORMAL		[Reference Range --> 4p16.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-3.5, Detected: Z<=-3.5]
4p16.2	0.0523609228112044	NORMAL		[Reference Range --> 4p16.2 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]
4p16.1	-1.81684861865404	NORMAL		[Reference Range --> 4p16.1 cytoband (Z-score): Normal: Z>-5, Suspected: -5.0>=<Z>-8.0, Detected: Z<=-8.0]
4p15.33	-1.78862881521613	NORMAL		[Reference Range --> 4p15.33 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-6.0, Detected: Z<=-6.0]
4p15.32	-2.022704830461	NORMAL		[Reference Range --> 4p15.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
4p15.31	-0.307494277897193	NORMAL		[Reference Range --> 4p15.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
4p15.2	-0.2907320397622	NORMAL		[Reference Range --> 4p15.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Cri-Du-Chat_Syndrome	NORMAL
AllBand	1.1665054223675	NORMAL		[Reference Range --> Multiple CDC associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-6.0, Detected: Z<=-6.0]
5p15.33	-0.483497803115129	NORMAL		[Reference Range --> 5p15.33 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
5p15.32	-1.64496206553085	NORMAL		[Reference Range --> 5p15.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
5p15.31	0.781158584802237	NORMAL		[Reference Range --> 5p15.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
5p15.2	1.28628553937164	NORMAL		[Reference Range --> 5p15.2 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]
5p15.1	0.769947254057826	NORMAL		[Reference Range --> 5p15.1 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]
5p14.3	0.906099692849801	NORMAL		[Reference Range --> 5p14.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
5p14.2	0.136942903729096	NORMAL		[Reference Range --> 5p14.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
5p14.1	1.41993009258828	NORMAL		[Reference Range --> 5p14.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
5p13.3	0.258431933043742	NORMAL		[Reference Range --> 5p13.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

WilliamsBeuren_Syndrome	NORMAL
7q11.23	-0.326759790083002	NORMAL		[Reference Range --> 7q11.23 cytoband (Z-score): Normal: Z>-2, Suspected: -2.0>=<Z>-2.5, Detected: Z<=-2.5]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Jacobsen_Syndrome	NORMAL
AllBand	0.01348264284706	NORMAL		[Reference Range --> Multiple Jacobsen_Syndrome associated cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
11q24.1	-0.488405574475971	NORMAL		[Reference Range --> 11q24.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
11q24.2	0.626199915966958	NORMAL		[Reference Range --> 11q24.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
11q24.3	-1.58253263569436	NORMAL		[Reference Range --> 11q24.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
11q25	-1.06514313519521	NORMAL		[Reference Range --> 11q25 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PraderWilli/Angelmen_Syndrome	NORMAL
AllBand	1.99001992385654	NORMAL		[Reference Range --> Multiple PraderWilli/Angelmen_Syndrome associated cytoband (Z-score): Normal: Z>-2.5, Suspected: -2.5>=<Z>-3.5, Detected: Z<=-3.5]
15q11.2	1.43306680124479	NORMAL		[Reference Range --> 15q11.2 cytoband (Z-score): Normal: Z>-2.5, Suspected: -2.5>=<Z>-3.5, Detected: Z<=-3.5]
15q12	1.98665726897784	NORMAL		[Reference Range --> 15q12 cytoband (Z-score): Normal: Z>-3, Suspected: -3>=<Z>-4, Detected: Z<=-4]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DiGeorge_Syndrome	NORMAL
22q11.21	0.452356483295834	NORMAL		[Reference Range --> 22q11.21 cytoband (Z-score): Normal: Z>-3, Suspected: -3>=<Z>-4, Detected: Z<=-4]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------