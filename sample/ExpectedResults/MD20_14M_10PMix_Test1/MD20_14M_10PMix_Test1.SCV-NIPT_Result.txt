Trisomy9	NORMAL
T9p	-0.0516775496200842	NORMAL		[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Trisomy13	NORMAL
Chr13	-0.130116575617998	NORMAL		[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Trisomy16	NORMAL
Chr16	1.88628407400798	NORMAL		[Reference Range (Z-score): Normal: Z<3.5, Suspected: 3.5=<Z<4, Detected: Z>=4]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Trisomy18	NORMAL
Chr18	-0.830064576981729	NORMAL		[Reference Range (Z-score): Normal: Z<6, Suspected: 6=<Z<8, Detected: Z>=8]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Trisomy21	NORMAL
Chr21	-1.25964009798384	NORMAL		[Reference Range --> Complete Chromosome 21 (Z-score): Normal: Z<4, Suspected: 4=<Z<5, Detected: Z>=5]
T21q	-1.17143747087654	NORMAL		[Reference Range --> T21q - q-arm of Chromosome 21 (Z-score): Normal: Z<4, Suspected: 4=<Z<5, Detected: Z>=5]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Trisomy22	NORMAL
T22	-0.175505310650377	NORMAL		[Reference Range --> Emanuel Syndrome (Z-score): Normal: Z<2.5, Suspected: 2.5=<Z<3.5, Detected: Z>=3.5]
Chr22	0.636350007864388	NORMAL		[Reference Range --> Complete Chromosome 22 (Z-score): Normal: Z<2.5, Suspected: 2.5=<Z<3.5, Detected: Z>=3.5]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Sex:	Male
Sex_Chromosomal_Anueploidy:	NORMAL
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

1p36_DeletionSyndrome	NORMAL
1p36	-0.849210659261041	NORMAL		[Reference Range --> Multiple 1p36DS associated cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
1p36.33	0.679137189146859	NORMAL		[Reference Range --> 1p36.33 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
1p36.32	-1.31537815043034	NORMAL		[Reference Range --> 1p36.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
1p36.31	-1.08259892549242	NORMAL		[Reference Range --> 1p36.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
1p36.23	-1.18826783502417	NORMAL		[Reference Range --> 1p36.23 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

2q33_DeletionSyndrome	NORMAL
2q33	0.152194434943179	NORMAL		[Reference Range --> Multiple 2q33DS associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]
2q31.1	-0.950654399246597	NORMAL		[Reference Range --> 2q31.1 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]
2q31.2	-0.675003143355044	NORMAL		[Reference Range --> 2q31.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
2q31.3	-0.891664950647541	NORMAL		[Reference Range --> 2q31.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
2q32.1	0.00528579860226226	NORMAL		[Reference Range --> 2q32.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
2q32.2	1.54994827460682	NORMAL		[Reference Range --> 2q32.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
2q32.3	-0.173181047429794	NORMAL		[Reference Range --> 2q32.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
2q33.1	-1.48237402570125	NORMAL		[Reference Range --> 2q33.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

WolfHirschhorn_Syndrome	NORMAL
AllBand	-1.5851178385819	NORMAL		[Reference Range --> Multiple WHS associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]
4p16.3	0.174757145725856	NORMAL		[Reference Range --> 4p16.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-3.5, Detected: Z<=-3.5]
4p16.2	0.585754147768401	NORMAL		[Reference Range --> 4p16.2 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]
4p16.1	-2.01258079384636	NORMAL		[Reference Range --> 4p16.1 cytoband (Z-score): Normal: Z>-5, Suspected: -5.0>=<Z>-8.0, Detected: Z<=-8.0]
4p15.33	-1.92536066623942	NORMAL		[Reference Range --> 4p15.33 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-6.0, Detected: Z<=-6.0]
4p15.32	-1.99330588004776	NORMAL		[Reference Range --> 4p15.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
4p15.31	-0.99088471137551	NORMAL		[Reference Range --> 4p15.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
4p15.2	-0.68523334136605	NORMAL		[Reference Range --> 4p15.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Cri-Du-Chat_Syndrome	NORMAL
AllBand	1.79310984163944	NORMAL		[Reference Range --> Multiple CDC associated cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-6.0, Detected: Z<=-6.0]
5p15.33	-0.133686558140568	NORMAL		[Reference Range --> 5p15.33 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
5p15.32	-1.38046270104197	NORMAL		[Reference Range --> 5p15.32 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
5p15.31	-0.00349742463539258	NORMAL		[Reference Range --> 5p15.31 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
5p15.2	0.61087734722697	NORMAL		[Reference Range --> 5p15.2 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]
5p15.1	0.115749652823308	NORMAL		[Reference Range --> 5p15.1 cytoband (Z-score): Normal: Z>-4, Suspected: -4.0>=<Z>-5.0, Detected: Z<=-5.0]
5p14.3	-0.167928161027228	NORMAL		[Reference Range --> 5p14.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
5p14.2	-0.520545958122233	NORMAL		[Reference Range --> 5p14.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
5p14.1	0.982636948538868	NORMAL		[Reference Range --> 5p14.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
5p13.3	0.707799471798057	NORMAL		[Reference Range --> 5p13.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

WilliamsBeuren_Syndrome	DETECTED
7q11.23	-3.25807661797186	DETECTED		[Reference Range --> 7q11.23 cytoband (Z-score): Normal: Z>-2, Suspected: -2.0>=<Z>-2.5, Detected: Z<=-2.5]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Jacobsen_Syndrome	NORMAL
AllBand	-0.046340842071721	NORMAL		[Reference Range --> Multiple Jacobsen_Syndrome associated cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
11q24.1	-0.309723777172687	NORMAL		[Reference Range --> 11q24.1 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
11q24.2	0.230178877650514	NORMAL		[Reference Range --> 11q24.2 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
11q24.3	-1.27799728997592	NORMAL		[Reference Range --> 11q24.3 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
11q25	-0.754563814353388	NORMAL		[Reference Range --> 11q25 cytoband (Z-score): Normal: Z>-3, Suspected: -3.0>=<Z>-4.0, Detected: Z<=-4.0]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PraderWilli/Angelmen_Syndrome	NORMAL
AllBand	0.774654309148004	NORMAL		[Reference Range --> Multiple PraderWilli/Angelmen_Syndrome associated cytoband (Z-score): Normal: Z>-2.5, Suspected: -2.5>=<Z>-3.5, Detected: Z<=-3.5]
15q11.2	0.563441819623319	NORMAL		[Reference Range --> 15q11.2 cytoband (Z-score): Normal: Z>-2.5, Suspected: -2.5>=<Z>-3.5, Detected: Z<=-3.5]
15q12	0.365924770283942	NORMAL		[Reference Range --> 15q12 cytoband (Z-score): Normal: Z>-3, Suspected: -3>=<Z>-4, Detected: Z<=-4]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DiGeorge_Syndrome	NORMAL
22q11.21	1.02946684284577	NORMAL		[Reference Range --> 22q11.21 cytoband (Z-score): Normal: Z>-3, Suspected: -3>=<Z>-4, Detected: Z<=-4]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------